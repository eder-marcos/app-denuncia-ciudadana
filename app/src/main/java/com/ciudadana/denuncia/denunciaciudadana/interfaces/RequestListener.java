package com.ciudadana.denuncia.denunciaciudadana.interfaces;

import com.android.volley.VolleyError;

import java.text.ParseException;

/**
 * Created by Sferea.02 on 30/05/2017.
 */


/**
 * Interfaz que permite establecer comunicacion entre las clases que pertenecen al paquete com.prosa.web.requests y las que hacen uso de ellas.
 */
public interface RequestListener {

    /**
     * Metodo para detectar cuando una peticion al servicio web ha regresado la respuesta
     * @param response - respuesta que ha retornado la peticion al sw
     * @throws ParseException - lanza una excepcion en caso de ocurrir un error de parseo
     */
    public void onResponse(Object response);

    /**
     * Metodo para detectar cuando ha ocurrido una excepcion al momento de hacer una peticion al servicio web
     * @param error - Objeto que permite conocer el tipo de error que ha ocurrido al hacer la peticion
     */
    public void onError(VolleyError error);
}