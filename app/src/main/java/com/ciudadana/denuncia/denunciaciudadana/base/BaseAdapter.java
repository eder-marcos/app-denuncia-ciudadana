package com.ciudadana.denuncia.denunciaciudadana.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by Sferea.02 on 23/05/2017.
 */

public abstract class BaseAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    protected ArrayList<T> mList;
    protected Context mContext;
    protected AdapterView.OnItemClickListener mClickListener;
    protected LinkedHashMap<String, Integer> sectionsMap;
    protected int[] sectionForPosition;
    protected ArrayList<Integer> positionForSection;

    /**
     * El constructor
     *
     * @param context
     */
    public BaseAdapter(Context context) {
        this.mContext = context;
        mList = new ArrayList<>();
    }

    /**
     * @param parent
     * @param viewType
     * @return
     */
    public abstract VH onCreateViewHolder(ViewGroup parent, int viewType);

    /**
     * @param holder
     * @param position
     */
    public abstract void onBindViewHolder(VH holder, int position);

    /**
     * @return
     */
    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }

    /**
     * @param listener
     */
    public void setOnClickListener(AdapterView.OnItemClickListener listener) {
        mClickListener = listener;
    }

    /**
     *
     */
    public void clear() {
        mList.clear();
        notifyDataSetChanged();
    }

    /**
     * @param agendaList
     */
    public void addAll(ArrayList<T> agendaList) {
        this.mList = agendaList;
        notifyDataSetChanged();
    }

    /**
     *
     */
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        /**
         * @param v
         */
        @Override
        public void onClick(View v) {
            if (mClickListener != null)
                mClickListener.onItemClick(null, v, getLayoutPosition(), getItemId());
        }
    }
}
