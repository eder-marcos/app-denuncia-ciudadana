package com.ciudadana.denuncia.denunciaciudadana.web.receiver;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.ciudadana.denuncia.denunciaciudadana.interfaces.RequestListener;
import com.ciudadana.denuncia.denunciaciudadana.models.Utils.Response;
import com.ciudadana.denuncia.denunciaciudadana.parser.Parser;
import com.ciudadana.denuncia.denunciaciudadana.web.request.Request;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Sferea.02 on 30/05/2017.
 */

/**
 * Hilo que gestiona la petición al servicio web. En caso de ser exitosa, llama al método
 * onResponse() de la clase que la invocó, en caso contrario, llama a onError() indicando la falla. Si el
 * hilo es cancelado, retira la petición de la cola.
 */
public class Receiver extends AsyncTask<Object, Void, Object> implements RequestListener, DialogInterface.OnCancelListener {
    private RequestListener mRequestListener;
    private Request.Method mRequestType;
    private Parser.ParserType mParserType;
    private Class mClassType;
    private SwipeRefreshLayout mSwipeRefreshAnimator;
    private ProgressDialog mProgressDialog;
    private Object mResponse;
    private boolean mReceived;
    private VolleyError volleyError;
    private String mDialogMessage;
    private Object[] mRequestData;
    private boolean mIsUsingDialog;

    /**
     * Constructor que permite inicializar al hilo. En este caso, se le indicará al
     * usuario que se esta ejecutando la petición mediante un SwipeRefreshLayout.
     *
     * @param listener             contexto en el que se llamó.
     * @param requestType          Tipo de petición, 0 - Post, 1 - Get.
     * @param swipeRefreshAnimator animación inicializada de la clase donde se llamó.
     * @param type                 tipo de clase de la que se va a recibir la información.
     */
    public Receiver(RequestListener listener, Request.Method requestType, Parser.ParserType parserType, Class type, SwipeRefreshLayout swipeRefreshAnimator) {
        this.mRequestListener = listener;
        this.mRequestType = requestType;
        this.mParserType = parserType;
        this.mClassType = type;
        this.mSwipeRefreshAnimator = swipeRefreshAnimator;
    }

    /**
     * Constructor que permite inicializar al hilo. En este caso, se le indicará al
     * usuario que se esta ejecutando la petición mediante un ProgressDialog.
     *
     * @param listener contexto en el que se llamó.
     * @param type     tipo de clase de la que se va a recibir la información.
     */
    public Receiver(RequestListener listener, Request.Method requestType, Parser.ParserType parserType, Class type, String progressDialogMessage) {
        this.mRequestListener = listener;
        this.mRequestType = requestType;
        this.mParserType = parserType;
        this.mClassType = type;
        this.mDialogMessage = progressDialogMessage;
        this.mIsUsingDialog = true;
        //Context context = listener instanceof AppCompatActivity ? (Context) listener : ((Fragment) listener).getContext() ;
        Context context = listener instanceof AppCompatActivity ? (Context) listener : listener instanceof Fragment ? ((Fragment) listener).getContext() : ((DialogFragment) listener).getContext();
        this.mProgressDialog = new ProgressDialog(context);
        this.mProgressDialog.setTitle("");
        this.mProgressDialog.setMessage(mDialogMessage);
        this.mProgressDialog.setCancelable(false);
//      this.mProgressDialog.setOnCancelListener(this);
    }

    public Receiver(RequestListener listener, Request.Method requestType, Parser.ParserType parserType, Class type) {
        this.mRequestListener = listener;
        this.mRequestType = requestType;
        this.mParserType = parserType;
        this.mClassType = type;
        //Context context = listener instanceof AppCompatActivity ? (Context) listener : ((Fragment) listener).getContext() ;
        Context context = listener instanceof AppCompatActivity ? (Context) listener : listener instanceof Fragment ? ((Fragment) listener).getContext() : ((DialogFragment) listener).getContext();
    }

    /**
     * Muestra las animaciones de carga en el fragmento según sea el caso.
     */
    @Override
    public void onPreExecute() {
        showLoadAnimation(true);
    }

    /**
     * Pone la petición en cola y espera a la respuesta.
     * <p>
     * En caso de cancelarse el hilo, se cancela la petición.
     *
     * @param params [0] URL de la petición web.
     *               [1] nodo de donde se quiere sacar la información.
     *               [2] parámetros según el tipo de petición.
     * @return lista con los elementos recuperados del servicio web.
     */
    @Override
    public Object doInBackground(Object... params) {
        mRequestData = params;
        mResponse = new Response();
        Request request = new Request(mParserType, mClassType);
        int paramsIndex = 2;
        String url = (String) params[0];
        String rootNode = (String) params[1];
        Object requestParams = null;
        switch (mRequestType) {
            case POST:
                Map<String, String> postParams;
                if (params.length > paramsIndex) {
                    postParams = new HashMap<>();
                    for (int i = paramsIndex; i < params.length; i += 2)
                        postParams.put((String) params[i], (String) params[i + 1]);
                    requestParams = postParams;
                }
                break;
            case PUT:
                Map<String, String> putParams = null;
                if (params.length > paramsIndex) {
                    putParams = new HashMap<String, String>();
                    for (int i = paramsIndex; i < params.length; i += 2)
                        putParams.put((String) params[i], (String) params[i + 1]);
                    requestParams = putParams;
                }
                break;
            case GET:
                String getParams = "";
                if (params.length > paramsIndex) {
                    getParams += "?";
                    for (int i = paramsIndex; i < params.length; i += 2)
                        getParams += params[i] + "=" + params[i + 1];
                    getParams = getParams.substring(0, getParams.length() - 1);
                    requestParams = getParams;
                }
                break;
            case JSON:
                String jsonParams;
                if (params.length > paramsIndex) {
                    jsonParams = (String) params[paramsIndex];
                    requestParams = jsonParams;
                }
                break;
        }
        request.makeRequest(rootNode, mRequestType, url, requestParams, this);
        while (!mReceived)
            if (isCancelled()) {
                request.cancelRequest();
                break;
            }
        return mResponse;
    }

    /**
     * Si la petición tuvo éxito, se envía la respuesta al
     * fragmento y se desactivan las animaciones de carga.
     *
     * @param response respuesta del servicio web.
     */
    @Override
    public void onPostExecute(Object response) {
        showLoadAnimation(false);
        if (mRequestListener != null)
            mRequestListener.onResponse(response);
    }

    /**
     * Si el fragmento se destruye, entra a segundo plano o se detiene,
     * el hilo es cancelado y se deshabilita la animación de carga.
     *
     * @param resultList respuesta del servicio web.
     */
    @Override
    public void onCancelled(Object resultList) {
        showLoadAnimation(false);
        if (mRequestListener != null)
            mRequestListener.onError(volleyError);
    }

    /**
     * Es llamado cuando se realizó una petición con éxito y
     * ya hay un resultado disponible. Cuando sucede esto,
     * la bandera de espera se hace verdadera y el hilo
     * sale del buclé de espera.
     *
     * @param response respuesta del servicio web.
     */
    @Override
    public void onResponse(Object response) {
        this.mResponse = response;
        mReceived = true;
    }

    /**
     * Es llamado cuando ocurre un error al realizar la
     * petición. Cuando sucede esto, se muestra el error
     * en el fragmento y se manda un resultado vacio.
     *
     * @param error error ocurrido durante la petición.
     */
    @Override
    public void onError(VolleyError error) {
        volleyError = error;
        if (error instanceof NetworkError || error instanceof NoConnectionError)
            volleyError = new VolleyError(Request.ErrorCode.NO_CONECTION);
        else if (error instanceof ServerError || error instanceof AuthFailureError)
            volleyError = new VolleyError(Request.ErrorCode.SERVER);
        else if (error instanceof TimeoutError)
            volleyError = new VolleyError(Request.ErrorCode.TIMEOUT);
        else if (error.networkResponse == null) {
            String errorMessage = error.getMessage().replace(" ", "");
            errorMessage = errorMessage.toUpperCase();
            volleyError = new VolleyError(errorMessage);
        }
        cancel(true);
    }

    @Override
    public void onCancel(DialogInterface dialogInterface) {
        volleyError = new VolleyError("Descarga cancelada");
        cancel(true);
    }

    private void showLoadAnimation(boolean refresh) {
        if (mSwipeRefreshAnimator != null && mSwipeRefreshAnimator.isRefreshing())
            mSwipeRefreshAnimator.setRefreshing(refresh);
        else if (mProgressDialog != null) {
            if (refresh)
                mProgressDialog.show();
            else
                mProgressDialog.dismiss();
        }
    }

    public Request.Method getRequestType() {
        return mRequestType;
    }

    public void setRequestType(Request.Method requestType) {
        mRequestType = requestType;
    }

    public Parser.ParserType getParserType() {
        return mParserType;
    }

    public void setParserType(Parser.ParserType parserType) {
        mParserType = parserType;
    }

    public Class getClassType() {
        return mClassType;
    }

    public void setClassType(Class classType) {
        mClassType = classType;
    }

    public SwipeRefreshLayout getSwipeRefreshAnimator() {
        return mSwipeRefreshAnimator;
    }

    public void setSwipeRefreshAnimator(SwipeRefreshLayout swipeRefreshAnimator) {
        mSwipeRefreshAnimator = swipeRefreshAnimator;
    }

    public ProgressDialog getProgressDialog() {
        return mProgressDialog;
    }

    public void setProgressDialog(ProgressDialog progressDialog) {
        mProgressDialog = progressDialog;
    }

    public String getDialogMessage() {
        return mDialogMessage;
    }

    public void setDialogMessage(String dialogMessage) {
        mDialogMessage = dialogMessage;
    }

    public Object[] getRequestData() {
        return mRequestData;
    }

    public void setRequestData(Object[] requestData) {
        mRequestData = requestData;
    }

    public boolean isUsingDialog() {
        return mIsUsingDialog;
    }

    public void setUsingDialog(boolean usingDialog) {
        mIsUsingDialog = usingDialog;
    }
}

