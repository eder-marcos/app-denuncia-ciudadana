package com.ciudadana.denuncia.denunciaciudadana.models.Utils;

import com.ciudadana.denuncia.denunciaciudadana.wrappers.ErrorWrapper;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Sferea.02 on 30/05/2017.
 * Un modelo es utilizado para representar los atributos del objeto que esta dentro de un array
 */

public class Response {

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("data")
    private Object data;

    private ErrorWrapper codigo;

    /**
     * Constructor vacio
     * */
    public Response() {

    }

    /**
     * Metodos GET y SET
     * */
    public Object getData()
    {
        return data;
    }

    public void setData(Object data)
    {
        this.data = data;
    }

    public ErrorWrapper getCodigo()
    {
        return codigo;
    }

    public void setCodigo(ErrorWrapper codigo)
    {
        this.codigo = codigo;
    }
}