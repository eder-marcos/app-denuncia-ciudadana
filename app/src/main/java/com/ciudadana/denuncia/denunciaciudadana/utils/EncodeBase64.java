package com.ciudadana.denuncia.denunciaciudadana.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by sferea-android on 14/07/2017.
 * Modified by (Sferea) Eder Marcos on 01/08/2017
 */

public class EncodeBase64 {

    /**
     * Esta variable siempre es creada en todos los archivos para que cuando se realice DEBUG, se pueda filtrar en la consola con el valor de esta variable
     * */
    private static final String TAG = ConstantsUtils.DEBUG;

    /**
     * Declaracion de elementos del tipo view u otros
     * */

    /**
     * Declaracion de puras variables
     * */


    public EncodeBase64() {
        /**
         * Constructor vacio
         * */
    }

    /**
     * Funcion de Utileria
     * Codifica una imagen en Base64
     * */
    public static String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    /**
     * Funcion de Utileria
     * Codifica un Video en Base64
     * */
    public static String encodeVideo(String selectedImagePath) {
        FileInputStream objFileIS = null;
        try {
            System.out.println("file = >>>> <<<<<" + selectedImagePath);
            objFileIS = new FileInputStream(selectedImagePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream objByteArrayOS = new ByteArrayOutputStream();
        byte[] byteBufferString = new byte[1024];
        try {
            for (int readNum; (readNum = objFileIS.read(byteBufferString)) != -1;) {
                objByteArrayOS.write(byteBufferString, 0, readNum);
                System.out.println("read " + readNum + " bytes,");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        String videodata = Base64.encodeToString(objByteArrayOS.toByteArray(), Base64.DEFAULT);
        return videodata;
    }

    /**
     * Funcion de Utileria
     * Decodifica una imagen en Base64
     * */
    public static Bitmap decodeImage(String input) {
        byte[] decodedBytes = Base64.decode(input.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

    /**
     * Funcion de Utileria
     * Codifica varias imagenes imagen en Base64
     * */
    private static String encodeImages(String path) {
        File imagefile = new File(path);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(imagefile);
        } catch(FileNotFoundException e){
            e.printStackTrace();
        }
        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] b = baos.toByteArray();
        return  Base64.encodeToString(b, Base64.DEFAULT);
    }
}
