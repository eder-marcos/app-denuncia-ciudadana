package com.ciudadana.denuncia.denunciaciudadana.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.base.ReceiverFragment;

import jp.wasabeef.glide.transformations.BlurTransformation;


/**
 * Created by Eder Marcos.
 */

public class ProfileFragment extends ReceiverFragment {

    /**
     * Variable estatica creada para identificar lo que se imprime en consola y debuguear de mejor manera
     * */

    /**
     * Variables
     * */

    /**
     * Variables que son elementos de android
     * */
    private ImageView mBackground;
    private ImageView mProfile;
    private ImageView mIcSetting;
    private ImageView mIcEdit;

    /**
     * Funciones sobre escrita
     * */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_profile, container, false);
        /**
         * Localizar elementos
         * */
        ((TextView) mFragmentView.findViewById(R.id.tvToolbarTitleP)).setText("Mi perfil");
        mBackground = mFragmentView.findViewById(R.id.ivBackground);
        mProfile = mFragmentView.findViewById(R.id.ivImageProfile);
        mIcSetting = mFragmentView.findViewById(R.id.ivIcSettings);
        mIcEdit = mFragmentView.findViewById(R.id.ivIcEdit);

        /**
         * Se inicializan las variables
         * */
        int colorWhite = Color.parseColor("#FFFFFF");
        Glide.with(mContext)
                .load(R.drawable.bg_5_2)
                .bitmapTransform(new BlurTransformation(mContext))
                .into(mBackground);
        Glide.with(mContext).load(R.drawable.ic_man).into(mProfile);

        mIcSetting.setImageDrawable(getResources().getDrawable(R.drawable.ic_settings));
        mIcSetting.setColorFilter(colorWhite);
        mIcEdit.setImageDrawable(getResources().getDrawable(R.drawable.ic_edit));
        mIcEdit.setColorFilter(colorWhite);


        /**
         * Acciones de los botones de la parte inferior
         * */

        return mFragmentView;
    }

    /**
     * Respuesta del servidor
     * */
    @Override
    public void onResponse(Object response) {
        /**
         * Funcion vacia
         * */
    }
}