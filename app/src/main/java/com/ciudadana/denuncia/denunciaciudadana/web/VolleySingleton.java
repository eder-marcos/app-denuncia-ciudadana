package com.ciudadana.denuncia.denunciaciudadana.web;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.Volley;
import com.ciudadana.denuncia.denunciaciudadana.DenunciaCiudadana;

/**
 * Created by Sferea.02 on 30/05/2017.
 */


/**
 * Singleton que gestiona las peticiones a través de una cola administrada
 * por Volley.
 */
public class VolleySingleton {

    private static VolleySingleton mInstance;
    private RequestQueue mRequestQueue;

    private VolleySingleton() {
        getRequestQueue();
    }

    /**
     * Inicializa, en caso de que no exista, la única instancia de la clase.
     *
     * @return instancia de la clase.
     */
    public static synchronized VolleySingleton getInstance() {
        if (mInstance == null)
            mInstance = new VolleySingleton();
        return mInstance;
    }

    /**
     * Inicializa la cola de peticiones, si esta no existe, y devuelve la instancia.
     *
     * @return
     */
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null)
            mRequestQueue = Volley.newRequestQueue(DenunciaCiudadana.mApp);
        return mRequestQueue;
    }

    /**
     * Agrega una petición a la cola.
     *
     * @param req petición.
     */
    public void addToRequestQueue(Request req) {
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        req.setRetryPolicy(policy);
        getRequestQueue().add(req);
    }
}
