package com.ciudadana.denuncia.denunciaciudadana.wrappers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Sferea.02 on 30/05/2017.
 * Este wrapper sirve para parsear el objeto que se recibe dentro de data
 * Por lo general se recibe un ArrayList de un modelo en especifico.
 * En ocaciones se recibe datos adicionales como la cantidad, paginas, etc.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorWrapper {

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonProperty("codigo")
    private String mErrorCode;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonProperty("msg")
    private String mMessageError;

    /**
     * Constructor vacio
     * */
    public ErrorWrapper() {

    }

    /**
     * Constructor
     * */
    public ErrorWrapper(String errorCode, String messageError) {
        mErrorCode = errorCode;
        mMessageError = messageError;
    }

    /**
     * Metodos GET y SET
     * */
    public String getErrorCode()
    {
        return mErrorCode;
    }

    public void setErrorCode(String errorCode)
    {
        mErrorCode = errorCode;
    }

    public String getMessageError()
    {
        return mMessageError;
    }

    public void setMessageError(String messageError)
    {
        mMessageError = messageError;
    }
}