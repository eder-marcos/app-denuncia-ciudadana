package com.ciudadana.denuncia.denunciaciudadana.wrappers;

import com.ciudadana.denuncia.denunciaciudadana.models.ComplaintModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Eder Marcos.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ComplaintWrapper {

    /**
     * Este es l nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * Para cuando responda la peticion de tipo de transporte
     * */
    @JsonProperty("complaint")
    private ComplaintModel mComplaint;

    public ComplaintWrapper() {
        /**
         * Funcion vacia
         * */
    }

    public ComplaintModel getmComplaint() {
        return mComplaint;
    }

    public void setmComplaint(ComplaintModel mComplaint) {
        this.mComplaint = mComplaint;
    }
}
