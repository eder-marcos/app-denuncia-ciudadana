package com.ciudadana.denuncia.denunciaciudadana.wrappers;

import com.ciudadana.denuncia.denunciaciudadana.models.UserModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Eder Marcos.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserWrapper {
    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("profile")
    private UserModel mUser;

    public UserWrapper() {
        /**
         * Funcion vacia
         * */
    }

    public UserModel getmUser() {
        return mUser;
    }

    public void setmUser(UserModel mUser) {
        this.mUser = mUser;
    }
}
