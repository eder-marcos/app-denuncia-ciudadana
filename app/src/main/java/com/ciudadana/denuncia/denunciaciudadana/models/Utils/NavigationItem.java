package com.ciudadana.denuncia.denunciaciudadana.models.Utils;

/**
 * Created by (Sferea) Eder Marcos on 16/08/2017 at 19.
 * Un modelo es utilizado para representar los atributos del objeto que esta dentro de un array
 */

public class NavigationItem {

    /**
     * Atributos indispensables para poder dibujar un item dentro del menu
     * */
    private String mTitle;
    private String mBlock;
    private boolean isBlue;

    /**
     * Constructor vacio
     * */
    public NavigationItem() {
        /**
         * Constructor vacio
         * */
    }

    /**
     * Constructor
     * */
    public NavigationItem(String mTitle, String mBlock, boolean isBlue) {
        this.mTitle = mTitle;
        this.mBlock = mBlock;
        this.isBlue = isBlue;
    }

    /**
     * Metodos GET y SET
     * */
    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmBlock() {
        return mBlock;
    }

    public void setmBlock(String mBlock) {
        this.mBlock = mBlock;
    }

    public boolean isBlue() {
        return isBlue;
    }

    public void setBlue(boolean blue) {
        isBlue = blue;
    }
}
