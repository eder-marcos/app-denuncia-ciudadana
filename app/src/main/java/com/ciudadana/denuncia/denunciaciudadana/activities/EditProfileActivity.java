package com.ciudadana.denuncia.denunciaciudadana.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.base.ReceiverActivity;
import com.ciudadana.denuncia.denunciaciudadana.utils.CameraUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.ConstantsUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.PermissionsUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.SnackbarUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.StoreSharedPreferencesUtils;

import java.io.File;

import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Eder Marcos.
 */

public class EditProfileActivity extends ReceiverActivity {

    /**
     * Variables
     * */
    private static final String TAG = ConstantsUtils.DEBUG;
    private static final int PERMISSION_CALLBACK_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private  String[] mPermissions = new String[]{
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private StoreSharedPreferencesUtils mStore;
    private boolean sentToSettings = false;

    /**
     * UI Elementos de android
     * */
    private ImageView mIconBack;
    private ImageView mImageProfile;
    private ImageView mBackground;
    private ImageView mIconSave;
    private EditText mName;
    private EditText mRole;
    private EditText mEmail;
    private EditText mPhone;
    private View mView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * Localizacion de elementos
         * */
        mIconBack = findViewById(R.id.ivIcSingle);
        mIconSave = findViewById(R.id.ivIcSingle2);
        mBackground = findViewById(R.id.ivBackground);
        mImageProfile = findViewById(R.id.ivImageProfileEdit);
        mName = findViewById(R.id.etName);
        mRole = findViewById(R.id.etLastName);
        mEmail = findViewById(R.id.etEmail);
        mPhone = findViewById(R.id.etPhone);
        mView = findViewById(R.id.main_content_edit);

        /**
         * Se inicializan las variables
         * */
        mIconBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back));
        mIconBack.setColorFilter(Color.parseColor("#ffffff"));
        mIconSave.setImageDrawable(getResources().getDrawable(R.drawable.ic_save));
        mIconSave.setColorFilter(Color.parseColor("#ffffff"));

        mIconBack.setOnClickListener(letBackActivity);
        mIconSave.setOnClickListener(letSaveData);
        mImageProfile.setOnClickListener(letTakePicture);

        Glide.with(this)
                .load(R.drawable.bg_5_2)
                .bitmapTransform(new BlurTransformation(this))
                .into(mBackground);

        mStore = new StoreSharedPreferencesUtils(this);
    }

    /**
     * Variable
     * Variable del tipo onClickListener
     * */
    View.OnClickListener letBackActivity = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    View.OnClickListener letSaveData = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    View.OnClickListener letTakePicture = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (PermissionsUtils.check(
                    EditProfileActivity.this,
                    mPermissions[0]) ||
                    PermissionsUtils.check(
                            EditProfileActivity.this,
                            mPermissions[1])) {
                if (PermissionsUtils.rationale(
                        EditProfileActivity.this,
                        mPermissions[0]) ||
                        PermissionsUtils.rationale(
                                EditProfileActivity.this,
                                mPermissions[1])) {
                    PermissionsUtils.alertDialog(
                            EditProfileActivity.this,
                            mPermissions,
                            PERMISSION_CALLBACK_CONSTANT,
                            "Se requieren permisos",
                            "Esta aplicacion requiere permisos para poder utilizar la camara",
                            "Aceptar",
                            "Cancelar"
                    );
                } else if (mStore.getData(Manifest.permission.CAMERA, false)) {
                    sentToSettings = PermissionsUtils.alertDialogSettings(
                            EditProfileActivity.this,
                            mPermissions,
                            REQUEST_PERMISSION_SETTING,
                            "Se requieren permisos",
                            "Esta aplicacion requiere permisos para poder utilizar la camara",
                            "Aceptar",
                            "Cancelar"
                    );
                } else {
                    ActivityCompat.requestPermissions(EditProfileActivity.this, mPermissions, PERMISSION_CALLBACK_CONSTANT);
                }
                mStore.saveData(Manifest.permission.CAMERA, true);
            } else {
                CameraUtils.cameraRequest(EditProfileActivity.this, REQUEST_PERMISSION_SETTING);
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == PERMISSION_CALLBACK_CONSTANT) {
            boolean allgranted = false;
            for(int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    allgranted = true;
                } else {
                    allgranted = false;
                }
            }

            if (allgranted) {
                CameraUtils.cameraRequest(this, REQUEST_PERMISSION_SETTING);
            } else if (PermissionsUtils.rationale(
                    EditProfileActivity.this,
                    mPermissions[0]) ||
                    PermissionsUtils.rationale(
                            EditProfileActivity.this,
                            mPermissions[1])) {
                PermissionsUtils.alertDialog(
                        EditProfileActivity.this,
                        mPermissions,
                        PERMISSION_CALLBACK_CONSTANT,
                        "Se requieren permisos",
                        "Esta aplicacion requiere permisos para poder utilizar la camara",
                        "Aceptar",
                        "Cancelar"
                );
            } else {
                SnackbarUtils.showSnackbar(this, mView, "No se concedieron permisos", R.color.color_pomegranate);
            }
        }
    }

    /**
     * Pone la imágen en el cuadro de imágen que regresa la captura de cámara
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == Activity.RESULT_OK) {
                /**
                 * Cada ImageView tiene un codigo de peticion
                 * */
                switch (requestCode) {
                    case REQUEST_PERMISSION_SETTING:
                        /**
                         * Peticion cuando regresa de configuracion
                         * */
                        proceedAfterPermission();
                        break;

                    default:
                        SnackbarUtils.showSnackbar(this, mView, "Codigo de error desconocido", R.color.color_pomegranate);
                        break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Error en onActivityResult " + e.toString());
            SnackbarUtils.showSnackbar(this, mView, "Error en onActivityResult", R.color.color_pomegranate);
        }
    }

    private void proceedAfterPermission() {
        File mFile = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
        Glide.with(this)
                .load(mFile)
                .bitmapTransform(new CropCircleTransformation(this))
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(mImageProfile);
//        mImageProfile.setImageBitmap(CameraUtils.decodeSampledBitmapFromFile(mFile.getAbsolutePath(), 500, 250));
    }

    /**
     * Funcion sobre escrita
     * Cuando se otorgan permisos desde la configuracion, al regresar a la aplicacion, comprueba los permisos y si existen, se abre la camara
     * */
    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings) {
            if (PermissionsUtils.check(
                    EditProfileActivity.this,
                    mPermissions[0]) ||
                    PermissionsUtils.check(
                            EditProfileActivity.this,
                            mPermissions[1])) {
                CameraUtils.cameraRequest(EditProfileActivity.this, REQUEST_PERMISSION_SETTING);
            }
        }
    }

    /**
     * Funcion sobre escrita
     * Layout de la actividad
     * */
    @Override
    public int getLayoutResource() {
        return R.layout.activity_edit_profile;
    }

    /**
     * Funcion sobre escrita
     * Respuesta del servidor
     * */
    @Override
    public void onResponse(Object response) {
        /**
         * Funcion vacia
         * */
    }

}
