package com.ciudadana.denuncia.denunciaciudadana.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.adapters.FeedAdapter;
import com.ciudadana.denuncia.denunciaciudadana.adapters.NotificationAdapter;
import com.ciudadana.denuncia.denunciaciudadana.base.ReceiverFragment;
import com.ciudadana.denuncia.denunciaciudadana.models.NotificationModel;
import com.ciudadana.denuncia.denunciaciudadana.utils.ConvertTo;
import com.ciudadana.denuncia.denunciaciudadana.utils.GridSpacingItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eder Marcos.
 */

public class NotificationsFragment extends ReceiverFragment {

    /**
     * Variable estatica creada para identificar lo que se imprime en consola y debuguear de mejor manera
     * */

    /**
     * Variables
     * */
    private NotificationModel mModel;
    private List<NotificationModel> mList;
    private NotificationAdapter mAdapter;

    /**
     * Variables que son elementos de android
     * */
    private RecyclerView mRecyclerView;
    private ImageView mIcClear;

    /**
     * Funciones sobre escrita
     * */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_notifications, container, false);
        ((TextView) mFragmentView.findViewById(R.id.tvToolbarTitleN)).setText("Notificaciones");
        /**
         * Localizar elementos
         * */
        mRecyclerView = mFragmentView.findViewById(R.id.rvNotifications);
        mIcClear = mFragmentView.findViewById(R.id.ivIcClear);

        /**
         * Se inicializan las variables
         * */
        int colorWhite = Color.parseColor("#FFFFFF");
        mIcClear.setImageDrawable(getResources().getDrawable(R.drawable.ic_clear_all));
        mIcClear.setColorFilter(colorWhite);

        mList = new ArrayList<>();
        mAdapter = new NotificationAdapter(mList, getContext());

        mModel = new NotificationModel(
                R.drawable.ic_man,
                "Eder Marcos",
                "ha recibido la denuncia",
                "Denuncia 1",
                "Lunes a las 1:20 p.m."
        );
        mList.add(mModel);
        mModel = new NotificationModel(
                R.drawable.ic_man_2,
                "Maureen Fletcher",
                "ha leido la denuncia",
                "Denuncia 2",
                "Martes a las 08:20 a.m."
        );
        mList.add(mModel);
        mModel = new NotificationModel(
                R.drawable.ic_man_3,
                "Tracy Shelton",
                "esta revisando la denuncia",
                "Denuncia 3",
                "Miercoles a las 1:20 a.m."
        );
        mList.add(mModel);
        mModel = new NotificationModel(
                R.drawable.ic_girl_1,
                "Bobby Young",
                "ha aprobado la denuncia",
                "Denuncia 4",
                "Jueves a las 11:20 a.m."
        );
        mList.add(mModel);
        mModel = new NotificationModel(
                R.drawable.ic_girl_2,
                "Joshua miller",
                "ha rechazado",
                "Denuncia 5",
                "Viernes a las 4:20 p.m."
        );
        mList.add(mModel);


        mAdapter.notifyDataSetChanged();
        /**
         * Configuracion del recycler view
         * */
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 1);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(
                new GridSpacingItemDecoration(1,
                        ConvertTo.dpToPx(5,
                                mContext.getResources()),
                        true
                ));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        return mFragmentView;
    }

    /**
     * Respuesta del servidor
     * */
    @Override
    public void onResponse(Object response) {
        /**
         * Funcion vacia
         * */
    }
}
