package com.ciudadana.denuncia.denunciaciudadana.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created Eder Marcos.
 * Un modelo es utilizado para representar los atributos del objeto que esta dentro de un array
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClassificationModel {

    /**
     * Variables
     * */
    private String mTitle;
    private String mDescription;
    private int mIcon;

    /**
     * Funcion
     * Constructor vacio
     * */
    public ClassificationModel() {
        /**
         * Funcion vacia
         * */
    }

    /**
     * Funcion
     * Constructor
     * */
    public ClassificationModel(String mTitle, String mDescription, int mIcon) {
        this.mTitle = mTitle;
        this.mDescription = mDescription;
        this.mIcon = mIcon;
    }

    /**
     * Metodos GET y SET
     * */
    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public int getmIcon() {
        return mIcon;
    }

    public void setmIcon(int mIcon) {
        this.mIcon = mIcon;
    }
}
