package com.ciudadana.denuncia.denunciaciudadana.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;

/**
 * Created by (Sferea) Eder Marcos on 08/09/2017 at 16.
 */

public class PermissionsDialog {

    /**
     * Funcion de Utileria
     * */
    public static void showMessageWeNeedPermissions(final Context context) {
        /**
         * Muestra las configuraciones del dispositivo
         * Esta funcion pretende explicar porque se requiere los permisos
         * */
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Se requiere permisos");
        alertDialog.setMessage("Esta aplicacion necesita permisos para poder realizar esta accion");

        /**
         * Boton Aceptar
         * Si el usuario presion el boton de aceptar realiza los siguiente
         * Usa un intent para abrir las configuraciones
         * */
        alertDialog.setPositiveButton("ir a Configuracion", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                final Intent i = new Intent();
                i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                i.addCategory(Intent.CATEGORY_DEFAULT);
                i.setData(Uri.parse("package:" + context.getPackageName()));
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                context.startActivity(i);
            }
        });

        /**
         * Boton Cancelar
         * Si el usuario presion el boton de cancelar realiza los siguiente
         * Desaparece la alerta
         * */
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }
}
