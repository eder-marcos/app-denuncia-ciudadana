package com.ciudadana.denuncia.denunciaciudadana.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created Eder Marcos.
 * Un modelo es utilizado para representar los atributos del objeto que esta dentro de un array
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ComplaintModel implements Parcelable {

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("id")
    private String mId;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("fileNumber")
    private String mFileNumber;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("typeComplaint")
    private String mTipeComplaint;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("title")
    private String mTitle;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("description")
    private String mDescription;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("latitude")
    private String mLatitude;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("longitude")
    private String mLongitude;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("status")
    private String mStatus;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("state")
    private String mState;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("town")
    private String mTown;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("createdAt")
    private String mCreatedAt;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("updatedAt")
    private String mUpdatedAt;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("closedAt")
    private String mClosedAt;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("removed")
    private String mRemoved;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("completed")
    private String mCompleted;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("user_id")
    private String mUserId;

    private int mBgColor;
    private int mBgDrawable;
    private int mIcon;

    /**
     * Funcion
     * Constructor vacio
     * */
    public ComplaintModel() {
        /**
         * Funcion vacia
         * */
    }

    protected ComplaintModel(Parcel in) {
        mId = in.readString();
        mFileNumber = in.readString();
        mTipeComplaint = in.readString();
        mTitle = in.readString();
        mDescription = in.readString();
        mLatitude = in.readString();
        mLongitude = in.readString();
        mStatus = in.readString();
        mState = in.readString();
        mTown = in.readString();
        mCreatedAt = in.readString();
        mUpdatedAt = in.readString();
        mClosedAt = in.readString();
        mRemoved = in.readString();
        mCompleted = in.readString();
        mUserId = in.readString();
        mBgColor = in.readInt();
        mBgDrawable = in.readInt();
        mIcon = in.readInt();
    }

    public static final Creator<ComplaintModel> CREATOR = new Creator<ComplaintModel>() {
        @Override
        public ComplaintModel createFromParcel(Parcel in) {
            return new ComplaintModel(in);
        }

        @Override
        public ComplaintModel[] newArray(int size) {
            return new ComplaintModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mId);
        parcel.writeString(mFileNumber);
        parcel.writeString(mTipeComplaint);
        parcel.writeString(mTitle);
        parcel.writeString(mDescription);
        parcel.writeString(mLatitude);
        parcel.writeString(mLongitude);
        parcel.writeString(mStatus);
        parcel.writeString(mState);
        parcel.writeString(mTown);
        parcel.writeString(mCreatedAt);
        parcel.writeString(mUpdatedAt);
        parcel.writeString(mClosedAt);
        parcel.writeString(mRemoved);
        parcel.writeString(mCompleted);
        parcel.writeString(mUserId);
        parcel.writeInt(mBgColor);
        parcel.writeInt(mBgDrawable);
        parcel.writeInt(mIcon);
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmFileNumber() {
        return mFileNumber;
    }

    public void setmFileNumber(String mFileNumber) {
        this.mFileNumber = mFileNumber;
    }

    public String getmTipeComplaint() {
        return mTipeComplaint;
    }

    public void setmTipeComplaint(String mTipeComplaint) {
        this.mTipeComplaint = mTipeComplaint;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(String mLatitude) {
        this.mLatitude = mLatitude;
    }

    public String getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(String mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getmStatus() {
        return mStatus;
    }

    public void setmStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getmState() {
        return mState;
    }

    public void setmState(String mState) {
        this.mState = mState;
    }

    public String getmTown() {
        return mTown;
    }

    public void setmTown(String mTown) {
        this.mTown = mTown;
    }

    public String getmCreatedAt() {
        return mCreatedAt;
    }

    public void setmCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public String getmUpdatedAt() {
        return mUpdatedAt;
    }

    public void setmUpdatedAt(String mUpdatedAt) {
        this.mUpdatedAt = mUpdatedAt;
    }

    public String getmClosedAt() {
        return mClosedAt;
    }

    public void setmClosedAt(String mClosedAt) {
        this.mClosedAt = mClosedAt;
    }

    public String getmRemoved() {
        return mRemoved;
    }

    public void setmRemoved(String mRemoved) {
        this.mRemoved = mRemoved;
    }

    public String getmCompleted() {
        return mCompleted;
    }

    public void setmCompleted(String mCompleted) {
        this.mCompleted = mCompleted;
    }

    public String getmUserId() {
        return mUserId;
    }

    public void setmUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public int getmBgColor() {
        return mBgColor;
    }

    public void setmBgColor(int mBgColor) {
        this.mBgColor = mBgColor;
    }

    public int getmBgDrawable() {
        return mBgDrawable;
    }

    public void setmBgDrawable(int mBgDrawable) {
        this.mBgDrawable = mBgDrawable;
    }

    public int getmIcon() {
        return mIcon;
    }

    public void setmIcon(int mIcon) {
        this.mIcon = mIcon;
    }
}
