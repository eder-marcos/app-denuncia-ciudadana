package com.ciudadana.denuncia.denunciaciudadana.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Eder Marcos.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class MessagesModel implements Parcelable {

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("id")
    private String mId;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("message")
    private String mMessage;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("active")
    private boolean mActive;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("client_has_emergencyContact_id")
    private String mIdEmergencyContact;

    /**
     * Funcion
     * Constructor vacio
     * */
    public MessagesModel() {
        /**
         * Funcion vacia
         * */
    }

    protected MessagesModel(Parcel in) {
        mId = in.readString();
        mMessage = in.readString();
        mActive = in.readByte() != 0;
        mIdEmergencyContact = in.readString();
    }

    public static final Creator<MessagesModel> CREATOR = new Creator<MessagesModel>() {
        @Override
        public MessagesModel createFromParcel(Parcel in) {
            return new MessagesModel(in);
        }

        @Override
        public MessagesModel[] newArray(int size) {
            return new MessagesModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mId);
        parcel.writeString(mMessage);
        parcel.writeByte((byte) (mActive ? 1 : 0));
        parcel.writeString(mIdEmergencyContact);
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public boolean ismActive() {
        return mActive;
    }

    public void setmActive(boolean mActive) {
        this.mActive = mActive;
    }

    public String getmIdEmergencyContact() {
        return mIdEmergencyContact;
    }

    public void setmIdEmergencyContact(String mIdEmergencyContact) {
        this.mIdEmergencyContact = mIdEmergencyContact;
    }
}
