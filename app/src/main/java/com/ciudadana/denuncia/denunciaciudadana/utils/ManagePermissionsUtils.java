package com.ciudadana.denuncia.denunciaciudadana.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * Created by Skrapz on 20/09/2017.
 * Clase de Utileria
 * Permite gestionar los permisos que van a ser usados para utilizar diferentes componentes dentro de la aplicacion
 */

public class ManagePermissionsUtils {

    /**
     * Funcion de Utileria
     * @param activity: Actividad en la que se va a ejecutar
     * @param mPermission: Permiso que se piensa solicitar
     * @return Retorna un true, si tiene permisos
     * */
    public static boolean getPermission(Activity activity, String mPermission, int mRequest) {
        if (ContextCompat.checkSelfPermission(activity, mPermission)
                != PackageManager.PERMISSION_GRANTED) {
            /**
             * Muestra una explicacion del porque son necesarios los permisos
             * */
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, mPermission)) {
                /**
                 * todo Mostrar un Snackbar con una explicacion y un boton a los ajustes
                 * */
            } else {
                /**
                 * Solicita permisos
                 * */
                ActivityCompat.requestPermissions(
                        activity,
                        new String[]{mPermission},
                        mRequest);
            }
            return false;
        } else {
            return true;
        }
    }
}
