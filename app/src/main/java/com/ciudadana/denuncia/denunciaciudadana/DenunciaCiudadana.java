package com.ciudadana.denuncia.denunciaciudadana;

import android.app.Application;

/**
 * Created by Eder Marcos.
 */

public class DenunciaCiudadana extends Application {

    /**
     * Variable estatica creada para identificar lo que se imprime en consola y debuguear de mejor manera
     * */

    /**
     * Variables
     * */
    public static DenunciaCiudadana mApp;

    /**
     * Declaracion de elementos que pertenecen a Android
     * */

    /**
     * Funciones constructor
     * */
    public DenunciaCiudadana() {
        /**
         * Constructor
         * */
    }

    /**
     * Funciones sobre escrita
     * */
    @Override
    public void onCreate() {
        super.onCreate();
        mApp = this;
    }
}
