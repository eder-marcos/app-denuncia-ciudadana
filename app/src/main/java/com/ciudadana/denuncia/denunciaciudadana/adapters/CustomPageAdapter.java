package com.ciudadana.denuncia.denunciaciudadana.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.fragments.ComplaintFragment;
import com.ciudadana.denuncia.denunciaciudadana.interfaces.ActionDialogListener;
import com.ciudadana.denuncia.denunciaciudadana.models.ComplaintModel;
import com.ciudadana.denuncia.denunciaciudadana.models.UserModel;
import com.ciudadana.denuncia.denunciaciudadana.utils.ConvertTo;
import com.ciudadana.denuncia.denunciaciudadana.utils.GridSpacingItemDecoration;
import com.ciudadana.denuncia.denunciaciudadana.utils.StoreSharedPreferencesUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.ValidationsUtils;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eder Marcos.
 */

public class CustomPageAdapter extends PagerAdapter {

    /**
     * Variables
     * */
    private int[] mLayouts;
    private List<ComplaintModel> mList;
    private ComplaintAdapter mAdapterTypes;
    private StoreSharedPreferencesUtils mStore;

    /**
     * Declaracion de elementos que pertenecen a Android
     * */
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private ComplaintFragment mFragment;

    /**
     * Funcion
     * Constructor
     * */
    public CustomPageAdapter() {
        /**
         * Funcion vacia
         * */
    }

    /**
     * Funcion
     * Constructor
     * */
    public CustomPageAdapter(int[] mLayouts, Context context) {
        this.mLayouts = mLayouts;
        this.mContext = context;
    }

    public CustomPageAdapter(int[] mLayouts, Context context, ComplaintFragment fragment) {
        this.mLayouts = mLayouts;
        this.mContext = context;
        this.mFragment = fragment;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = mLayoutInflater.inflate(mLayouts[position], container, false);
        container.addView(view);
        mStore = new StoreSharedPreferencesUtils(mContext);
        if (position == 1) {
            /**
             * Set Titulo
             * */
            ((EditText) ((Activity)mContext).findViewById(R.id.etTitle2)).addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    /**
                     * Funcion vacia
                     * */
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    ((TextView) ((Activity)mContext).findViewById(R.id.tvContTitle)).setText(
                            "(" + Integer.toString(charSequence.length()) + "/100)"
                    );
                    ((TextView) ((Activity)mContext).findViewById(R.id.tvTitle3)).setText(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    /**
                     * Funcion vacia
                     * */
                }
            });

            ((EditText) ((Activity)mContext).findViewById(R.id.etDesc2)).addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    /**
                     * Funcion vacia
                     * */
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    ((TextView) ((Activity)mContext).findViewById(R.id.tvContDesc)).setText(
                            "(" + Integer.toString(charSequence.length()) + "/255)"
                    );
                    ((TextView) ((Activity)mContext).findViewById(R.id.tvDescription3)).setText(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    /**
                     * Funcion vacia
                     * */
                }
            });
        } else if (position == 2) {
            final String type = ((TextView) ((Activity)mContext).findViewById(R.id.tvTypeComplaint)).getText().toString();
            final String title = ((EditText) ((Activity)mContext).findViewById(R.id.etTitle2)).getText().toString();
            final String description = ((EditText) ((Activity)mContext).findViewById(R.id.etDesc2)).getText().toString().trim();
            String victim = ((TextView) ((Activity)mContext).findViewById(R.id.tvVictim)).getText().toString();

            ((TextView) ((Activity)mContext).findViewById(R.id.tvType3)).setText(type);
            ((TextView) ((Activity)mContext).findViewById(R.id.tvTitle3)).setText(title);
            ((TextView) ((Activity)mContext).findViewById(R.id.tvDescription3)).setText(description);
            ((TextView) ((Activity)mContext).findViewById(R.id.tvVictim3)).setText(victim);

            if (mStore.getData("drawableComplaint", 0) != 0) {
                Glide.with(mContext)
                        .load(mStore.getData("drawableComplaint", 0))
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(((ImageView) ((Activity) mContext).findViewById(R.id.ivIconTypeC)));
            } else {
                Glide.with(mContext)
                        .load(R.drawable.ic_add)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(((ImageView) ((Activity) mContext).findViewById(R.id.ivIconTypeC)));
            }

            /**
             * Subir evidencia
             * */
            final ImageView mEvidence1 = ((Activity) mContext).findViewById(R.id.ivEvidence1);
            mEvidence1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /**
                     * Almacenar datos
                     * */
                    ComplaintModel complaintModel = new ComplaintModel();
                    complaintModel.setmTipeComplaint(((TextView) ((Activity)mContext).findViewById(R.id.tvTypeComplaint)).getText().toString());
                    complaintModel.setmTitle(((TextView) ((Activity)mContext).findViewById(R.id.tvTitle3)).getText().toString());
                    complaintModel.setmDescription(((TextView) ((Activity)mContext).findViewById(R.id.tvDescription3)).getText().toString());
                    complaintModel.setmBgColor(mStore.getData("colorComplaint", 0));
                    complaintModel.setmBgDrawable(mStore.getData("drawableComplaint", 0));
                    complaintModel.setmUserId(mStore.getData("user_id", ""));

                    Log.d("TAG", "title: " + ((TextView) ((Activity)mContext).findViewById(R.id.tvTitle3)).getText().toString());
                    Log.d("TAG", "description: " + ((TextView) ((Activity)mContext).findViewById(R.id.tvDescription3)).getText().toString());

                    Gson gson = new Gson();
                    String json = gson.toJson(complaintModel);
                    mStore.saveData("complaintModel", json);

                    mFragment.openFileChooserDialog();
                }
            });

        }

        /**
         * Configuracion del RecyclerView
         * */
        mList = new ArrayList<>();
        mAdapterTypes = new ComplaintAdapter(mList, mContext);

        mRecyclerView = container.findViewById(R.id.recycler_view);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 2);
            mRecyclerView.setLayoutManager(mLayoutManager);
            mRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mRecyclerView.setAdapter(mAdapterTypes);
        initConfig();
        return view;
    }

    /**
     * Funcion de utileria
     * */
    public void setImage(Bitmap bm, int id) {
        ImageView iv = ((Activity) mContext).findViewById(id);
        iv.setImageBitmap(bm);
    }

    @Override
    public int getCount() {
        return mLayouts.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == obj;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }

    /**
     * Funcion
     * Funcion de utileria que inicializa la configuracion del RecyclerView
     * */
    private void initConfig() {
        int[] mColors = new int[] {
                R.color.color_green_sea,
                R.color.color_pomegranate,
                R.color.color_belize_hole,
                R.color.color_wisteria,
                R.color.color_concrete,
                R.color.color_pumpkin
        };
        ComplaintModel mModel = new ComplaintModel();
        mModel.setmTitle("Delitos Informaticos");
        mModel.setmDescription("");
        mModel.setmBgDrawable(R.drawable.ic_informatico);
        mModel.setmBgColor(mColors[0]);
        mList.add(mModel);

        mModel = new ComplaintModel();
        mModel.setmTitle("Robos");
        mModel.setmDescription("");
        mModel.setmBgDrawable(R.drawable.ic_robos);
        mModel.setmBgColor(mColors[1]);
        mList.add(mModel);

        mModel = new ComplaintModel();
        mModel.setmTitle("Extorsion");
        mModel.setmDescription("");
        mModel.setmBgDrawable(R.drawable.ic_extorsion);
        mModel.setmBgColor(mColors[2]);
        mList.add(mModel);

        mModel = new ComplaintModel();
        mModel.setmTitle("Estafas");
        mModel.setmDescription("");
        mModel.setmBgDrawable(R.drawable.ic_estafa);
        mModel.setmBgColor(mColors[3]);
        mList.add(mModel);

        mModel = new ComplaintModel();
        mModel.setmTitle("Secuestros");
        mModel.setmDescription("");
        mModel.setmBgDrawable(R.drawable.ic_secuestro);
        mModel.setmBgColor(mColors[4]);
        mList.add(mModel);

        mModel = new ComplaintModel();
        mModel.setmTitle("Delito Electoral");
        mModel.setmDescription("");
        mModel.setmBgDrawable(R.drawable.ic_electoral);
        mModel.setmBgColor(mColors[5]);
        mList.add(mModel);

        mAdapterTypes.notifyDataSetChanged();
    }
}
