package com.ciudadana.denuncia.denunciaciudadana.activities;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.adapters.ClassificationAdapter;
import com.ciudadana.denuncia.denunciaciudadana.models.ClassificationModel;
import com.ciudadana.denuncia.denunciaciudadana.utils.ConvertTo;
import com.ciudadana.denuncia.denunciaciudadana.utils.GridSpacingItemDecoration;

import java.util.ArrayList;
import java.util.List;

public class ClassificationActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private ClassificationAdapter mAdapter;
    private List<ClassificationModel> mList;
    /**
     * UI Elementos de Android
     * */
    private View mView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classification);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initCollapsingToolbar();

        mView = findViewById(R.id.main_contentClassification);
        mRecyclerView = findViewById(R.id.recycler_view);

        mList = new ArrayList<>();
        mAdapter = new ClassificationAdapter(mList, this, mView);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(1, ConvertTo.dpToPx(10, getResources()), true));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        init();

        try {
            Glide.with(this).load(R.drawable.bg_cover).into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializing collapsing toolbar
     * Will show and hide the toolbar title on scroll
     */
    private void initCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(" ");
        AppBarLayout appBarLayout = findViewById(R.id.appbar);
        appBarLayout.setExpanded(true);

        // hiding & showing the title when toolbar expanded & collapsed
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle(getString(R.string.app_name));
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
    }

    /**
     * Configura los elementos que seran pintados por el adaptador
     */
    private void init() {
        int[] mIcons = new int[] {
                R.drawable.ic_panic_1,
                R.drawable.ic_home_1,
                R.drawable.ic_about_2,
                R.drawable.ic_help
        };

        ClassificationModel mModel = new ClassificationModel();
        mModel.setmTitle("Botón de Pánico");
        mModel.setmDescription("Al presionar este boton, una alerta sera enviada a tus contactos de confianza");
        mModel.setmIcon(mIcons[0]);
        mList.add(mModel);

        mModel = new ClassificationModel();
        mModel.setmTitle("Inicio");
        mModel.setmDescription("Ingresa a panel inicial para comenzar a utilizar toda las caracteristicas");
        mModel.setmIcon(mIcons[1]);
        mList.add(mModel);

        mModel = new ClassificationModel();
        mModel.setmTitle("Soporte Técnico");
        mModel.setmDescription("Conoce al equipo de desarrolladores que es encargado de dar soporte a esta aplicacion");
        mModel.setmIcon(mIcons[2]);
        mList.add(mModel);

        mModel = new ClassificationModel();
        mModel.setmTitle("Ayuda");
        mModel.setmDescription("¿Necesitas ayuda? Conoce las caracteristas de esta aplicacion");
        mModel.setmIcon(mIcons[3]);
        mList.add(mModel);

        mAdapter.notifyDataSetChanged();
    }
}
