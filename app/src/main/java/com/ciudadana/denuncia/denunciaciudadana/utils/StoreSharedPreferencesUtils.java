package com.ciudadana.denuncia.denunciaciudadana.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by (Sferea) Eder Marcos on 28/08/2017 at 09.
 * Clase cuya funcion va ser la de almacenar valores, letras, etc en sharedPreferences
 * Con esto, se trata de evitar el duplicado de codigo
 */

public class StoreSharedPreferencesUtils {

    /**
     * Esta variable siempre es creada en todos los archivos para que cuando se realice DEBUG, se pueda filtrar en la consola con el valor de esta variable
     * */

    /**
     * Declaracion de elementos del tipo view u otros
     * */
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    /**
     * Declaracion de puras variables
     * */

    /**
     * Constructor
     * */
    public StoreSharedPreferencesUtils(Context mContext) {
        mSharedPreferences = mContext.getSharedPreferences(ConstantsUtils.PREFERENCES, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    /**
     * Funcion que guarda los datos de SharedPreferences
     * name: Id con el que se va a guardar el atributo
     * value: valor del atributo
     * Solo valores Cadenas de texto
     * */
    public void saveData(String name, String value) {
        mEditor.putString(name, value);
        mEditor.apply();
    }

    /**
     * Funcion que guarda los datos de SharedPreferences
     * name: Id con el que se va a guardar el atributo
     * value: valor del atributo
     * Solo valores Booleanos
     * */
    public void saveData(String name, boolean value) {
        mEditor.putBoolean(name, value);
        mEditor.apply();
    }

    /**
     * Funcion que guarda los datos de SharedPreferences
     * name: Id con el que se va a guardar el atributo
     * value: valor del atributo
     * Solo valores Enteros
     * */
    public void saveData(String name, int value) {
        mEditor.putInt(name, value);
        mEditor.apply();
    }

    /**
     * Funcion que obtiene los datos de SharedPreferences
     * name: Id con el que se guardo el atributo
     * option: String que se va a mostrar en caso de que no esxita alguna valor con ese id
     * Solo valores Cadenas de texto
     * */
    public String getData(String name, String option) {
        return mSharedPreferences.getString(name, option);
    }

    /**
     * Funcion que obtiene los datos de SharedPreferences
     * name: Id con el que se guardo el atributo
     * option: boolean que se va a mostrar en caso de que no esxita alguna valor con ese id
     * Solo valores Booleanos
     * */
    public boolean getData(String name, boolean option) {
        return mSharedPreferences.getBoolean(name, option);
    }

    /**
     * Funcion que obtiene los datos de SharedPreferences
     * name: Id con el que se guardo el atributo
     * option: int que se va a mostrar en caso de que no esxita alguna valor con ese id
     * Solo valores Enteros
     * */
    public int getData(String name, int option) {
        return mSharedPreferences.getInt(name, option);
    }

    /**
     * Funcion de Utileria
     * Retorna el Token
     * @param context: Contexto en el que se ejecuta
     * @return Retorna el token almacenado en SharedPreferences, este solo se usa para las peticiones
     * */
    public static String getToken(Context context) {
        StoreSharedPreferencesUtils mSave = new StoreSharedPreferencesUtils(context);
        return mSave.getData(ConstantsUtils.TEMP_TOKEN_PREFERENCES, "");
    }

    /**
     * Funcion de Utileria
     * Retorna el Token
     * @param context: Contexto en el que se ejecuta
     * @return Retorna el token almacenado en SharedPreferences
     * */
    public static String getTokenUser(Context context) {
        StoreSharedPreferencesUtils mSave = new StoreSharedPreferencesUtils(context);
        return mSave.getData(ConstantsUtils.TOKEN_PREFERENCES, "");
    }

    /**
     * Funcion de Utileria
     * Retorna el Token
     * @param context: Contexto en el que se ejecuta
     * @return Retorna el token almacenado en SharedPreferences, este solo se usa para las peticiones
     * */
    public static void setConfirmationOrder(Context context, String[] data) {
        StoreSharedPreferencesUtils mSave = new StoreSharedPreferencesUtils(context);
        mSave.saveData(ConstantsUtils.ORDER_NUMBER, data[0]);
        mSave.saveData(ConstantsUtils.DESTINATION, data[1]);
        mSave.saveData(ConstantsUtils.REGION, data[2]);
        mSave.saveData(ConstantsUtils.TOWN, data[3]);
        mSave.saveData(ConstantsUtils.CP, data[4]);
        mSave.saveData(ConstantsUtils.STREET, data[5]);
    }

    /**
     * Funcion de Utileria
     * @param context: Contexto en el que se ejecuta
     * @param keys: Nombre de las variables que se van a guardar
     * @param values: Valores de las variables que se van a guardar
     * */
    public static void setValues(Context context, String[] keys, String[] values) {
        StoreSharedPreferencesUtils mUtils = new StoreSharedPreferencesUtils(context);
        for (int i = 0; i < keys.length; i++)
            mUtils.saveData(keys[i], values[i]);
    }

    /**
     * Funcion de Utileria
     * Retorna multiples valores almacenados en el orden de las llaves (keys) obtenidas
     * @param context: Contexto en el que se ejecuta
     * @param keys: Nombre de las variables que se van a guardar
     * */
    public static String[] getValues(Context context, String[] keys) {
        StoreSharedPreferencesUtils mUtils = new StoreSharedPreferencesUtils(context);
        String[] mResponse = new String[keys.length];
        for (int i = 0; i < keys.length; i++) {
            mResponse[i] = mUtils.getData(keys[i], "");
        }
        return mResponse;
    }
}
