package com.ciudadana.denuncia.denunciaciudadana.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.fragments.ComplaintFragment;
import com.ciudadana.denuncia.denunciaciudadana.fragments.FeedFragment;
import com.ciudadana.denuncia.denunciaciudadana.fragments.NotificationsFragment;
import com.ciudadana.denuncia.denunciaciudadana.fragments.ProfileFragment;

public class MainActivity extends AppCompatActivity {

    /**
     * Variable estatica creada para identificar lo que se imprime en consola y debuguear de mejor manera
     * */

    /**
     * Variables
     * */

    /**
     * Declaracion de elementos que pertenecen a Android
     * */
    private Fragment mFragment = null;
    private View mContentView;
    private BottomNavigationView navigation;
    private View mFrameLayout;

    /**
     * Funciones sobre escrita
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFrameLayout = findViewById(R.id.content);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mFragment = new FeedFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content, mFragment)
                .addToBackStack(null)
                .commit();
    }

    /**
     * Variable
     * Variable del tipo BottomNavigationView
     * Administra las opciones del menu inferior
     * */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    setTitle(R.string.title_home);
                    mFrameLayout.setFitsSystemWindows(false);
                    mFragment = new FeedFragment();
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content, mFragment)
                            .addToBackStack(null)
                            .commit();

                    /**
                     * Oculta el menu inferior
                     * */
//                    navigation.setVisibility(View.GONE);
                    return true;

                case R.id.navigation_dashboard:
                    setTitle(R.string.title_dashboard);
                    mFrameLayout.setFitsSystemWindows(false);
                    mFragment = new ComplaintFragment();
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content, mFragment)
                            .addToBackStack(null)
                            .commit();

                    /**
                     * Oculta el menu inferior
                     * */
//                    navigation.setVisibility(View.GONE);
                    return true;

                case R.id.navigation_notifications:
                    setTitle(R.string.title_notifications);
                    mFrameLayout.setFitsSystemWindows(false);
                    mFragment = new NotificationsFragment();
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content, mFragment)
                            .addToBackStack(null)
                            .commit();

                    /**
                     * Oculta el menu inferior
                     * */
//                    navigation.setVisibility(View.GONE);
                    return true;

//                case R.id.navigation_config:
//                    setTitle(R.string.title_setting);
//                    mFrameLayout.setFitsSystemWindows(true);
//                    getFragmentManager()
//                            .beginTransaction()
//                            .replace(android.R.id.content, new SettingsFragment())
//                            .commit();
//                    startActivity(new Intent(MainActivity.this, optional.class));
//                    finish();

                    /**
                     * Oculta el menu inferior
                     * */
//                    navigation.setVisibility(View.GONE);
//                    return true;

                case R.id.navigation_profile:
                    setTitle(R.string.title_profile);
                    mFrameLayout.setFitsSystemWindows(false);
                    mFragment = new ProfileFragment();
                    getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.content, mFragment)
                            .addToBackStack(null)
                            .commit();

                    /**
                     * Oculta el menu inferior
                     * */
//                    navigation.setVisibility(View.GONE);
                    return true;
            }
            return false;
        }

    };
}
