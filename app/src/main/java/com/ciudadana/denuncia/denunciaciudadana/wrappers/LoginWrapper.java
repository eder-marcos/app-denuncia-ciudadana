package com.ciudadana.denuncia.denunciaciudadana.wrappers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Eder Marcos.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginWrapper {

    /**
     * Este es l nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * Para cuando responda la peticion de tipo de transporte
     * */
    @JsonProperty("token")
    String mToken;

    public LoginWrapper() {
        /**
         * Funcion vacia
         * */
    }

    public String getmToken() {
        return mToken;
    }

    public void setmToken(String mToken) {
        this.mToken = mToken;
    }
}
