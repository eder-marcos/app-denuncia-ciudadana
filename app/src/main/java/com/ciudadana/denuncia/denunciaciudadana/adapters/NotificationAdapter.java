package com.ciudadana.denuncia.denunciaciudadana.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.models.NotificationModel;

import java.util.List;

/**
 * Created by Eder Marcos.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    /**
     * Variable estatica creada para identificar lo que se imprime en consola y debuguear de mejor manera
     * */

    /**
     * Variables
     * */
    private List<NotificationModel> mList;

    /**
     * Declaracion de elementos que pertenecen a Android
     * */
    private Context mContext;

    /**
     * Funcion
     * Constructor
     * */
    public NotificationAdapter() {
        /**
         * Funcion vacia
         * */
    }

    /**
     * Funcion
     * Constructor
     * */
    public NotificationAdapter(List<NotificationModel> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    /**
     * Clase
     * Clase para especificar los elementos
     * */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImage;
        public TextView mText;
        public TextView mHour;
        public RelativeLayout mContainer;

        public MyViewHolder(View itemView) {
            super(itemView);
            mImage = itemView.findViewById(R.id.ivIconN);
            mText = itemView.findViewById(R.id.tvTitleN);
            mHour = itemView.findViewById(R.id.tvHourN);
            mContainer = itemView.findViewById(R.id.mContainer);
        }
    }

    /**
     * Funcion
     * Funcion sobre escrita que carga el layout del card
     * */
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_notifications_card, parent, false);

        return new NotificationAdapter.MyViewHolder(view);
    }

    /**
     * Funcion
     * Funcion sobre escrita
     * */
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NotificationModel mItem = mList.get(position);
        /**
         * Se establecen los valores correspondientes
         * */
        SpannableString mSpannableString =  new SpannableString(mItem.getmTitle());
        mSpannableString.setSpan(new StyleSpan(Typeface.BOLD), 0, mSpannableString.length(), 0);
        holder.mText.append(mSpannableString);
        holder.mText.append(" ");
        holder.mText.append(mItem.getmAction());
        holder.mText.append(" ");
        holder.mText.append("\"" + mItem.getmDescription() + "\"");
        holder.mHour.setText(mItem.getmHour());
        Glide.with(mContext).load(mItem.getmImage()).into(holder.mImage);
        /**
         * Deteccion del evento clic
         * */
        holder.mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Evento click", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * Funcion
     * Funcion sobre escrita
     * */
    @Override
    public int getItemCount() {
        return mList.size();
    }
}
