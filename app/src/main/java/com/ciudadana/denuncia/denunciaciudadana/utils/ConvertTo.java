package com.ciudadana.denuncia.denunciaciudadana.utils;

import android.content.res.Resources;
import android.util.TypedValue;

/**
 * Created by Eder Marcos.
 */

public class ConvertTo {

    /**
     * Funcion de Utileria
     * Convierte dp a px
     * */
    public static int dpToPx(int dp, Resources r) {
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
