package com.ciudadana.denuncia.denunciaciudadana.base;

import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.ciudadana.denuncia.denunciaciudadana.R;


/**
 * Created by Sferea.02 on 23/05/2017.
 */

/**
 * Clase que incializa el action bar en las actividades que la hereden.
 * <p/>
 * Para agregar el action bar a una actividad, se deben realizar las siguientes
 * tareas.
 * <p/>
 * 1. Extender esta clase en la actividad en la que se quiere el action bar.
 * 2. Sobrescribir el método getLayoutResource con el layout deseado.
 * 3. Establecer el nombre que se mostrará en el action bar con ayuda del
 * método setToolBarTitle.
 * 4. Habilitar el ícono de back mediante el método setBackIconAsDisplayName.
 */
public abstract class ToolbarActivity extends AppCompatActivity
{
//    protected Toolbar mToolbar;
//    protected TextView mToolbarTitle;
    protected SharedPreferences mPreferences;

    /**
     * Inicializar el layout de la aplicación con ayuda del método
     * getLayoutResource. Además inicializa el action bar y el
     * TextField que contendrá el título de la actividad.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
//        overridePendingTransition(R.anim.slide_in_from_right, R.anim.slide_out_to_left);
        setContentView(getLayoutResource());
//        mToolbar = (Toolbar) findViewById(R.id.Utilstoolbar);
//        setSupportActionBar(mToolbar);
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);

    }

    /**
     * Establece el título de la actividad con base en la cadena
     * pasada.
     *
     * @param title título que aparecerá en el action bar.
     */
//    public void setToolbarTitle(String title)
//    {
//        mToolbarTitle.setText(title);
//    }

    /**
     * Establece el título de la actividad con base en el identificador de un
     * recurso de tipo cadena (strings.xml).
     *
     * @param titleRes título que aparecerá en el action bar.
     */
//    public void setToolbarTitle(int titleRes)
//    {
//        mToolbarTitle.setText(titleRes);
//    }

    /**
     * Habilita el botón de retroceso en la actividad.
     */
    public void setBackIconAsDisplayHome()
    {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    /**
     * Método que se debe sobrescribir retornando el layout que se
     * quiera mostrar en la actividad.
     *
     * @return layout que se quiera mostrar en la actividad.
     */
    public abstract int getLayoutResource();

    public void setBackIconColor(int color, int idDrawable) {
        /**
         * Se agrego la opcion de pasar como parametro el id del drawable (icono) que se usara como boton de regresar
         * Por defecto sera el icono R.drawable.ic_back
         * */
        if (idDrawable == 0)
            idDrawable = R.drawable.ic_arrow_back;
        final Drawable upArrow = ActivityCompat.getDrawable(this, idDrawable);
        upArrow.setColorFilter(ActivityCompat.getColor(this, color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    public void setCancelIconColor(int color)
    {
        final Drawable cancelarIcon = ActivityCompat.getDrawable(this, R.drawable.ic_close);
        cancelarIcon.setColorFilter(ActivityCompat.getColor(this, color), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(cancelarIcon);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}