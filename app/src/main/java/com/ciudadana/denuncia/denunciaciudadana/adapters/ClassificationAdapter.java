package com.ciudadana.denuncia.denunciaciudadana.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.activities.MainActivity;
import com.ciudadana.denuncia.denunciaciudadana.activities.SupportActivity;
import com.ciudadana.denuncia.denunciaciudadana.models.ClassificationModel;
import com.ciudadana.denuncia.denunciaciudadana.utils.SnackbarUtils;

import java.util.List;

/**
 * Created Eder Marcos.
 */

public class ClassificationAdapter extends RecyclerView.Adapter<ClassificationAdapter.MyViewHolder> {

    /**
     * Variable estatica creada para identificar lo que se imprime en consola y debuguear de mejor manera
     * */

    /**
     * Variables
     * */
    private List<ClassificationModel> mList;

    /**
     * Declaracion de elementos que pertenecen a Android
     * */
    private Context mContext;
    private View mView;

    /**
     * Constructor
     * */
    public ClassificationAdapter(Context mContext, List<ClassificationModel> albumList) {
        this.mContext = mContext;
        this.mList = albumList;
    }

    public ClassificationAdapter(List<ClassificationModel> mList, Context mContext, View mView) {
        this.mList = mList;
        this.mContext = mContext;
        this.mView = mView;
    }

    /**
     * Clase
     * Clase MyViewHolder
     * */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitle;
        public TextView mDescription;
        public ImageView mThumbnail;
        public CardView mCardView;

        public MyViewHolder(View view) {
            super(view);
            mTitle = view.findViewById(R.id.tvTitleC);
            mThumbnail = view.findViewById(R.id.ivIconC);
            mCardView = view.findViewById(R.id.cvClassification);
            mDescription = view.findViewById(R.id.tvDescriptionC);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_classification_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ClassificationModel mOption = mList.get(position);
        /**
         * Se establecen los valores correspondientes
         * */
        if (position == 0) {
            holder.mCardView.setCardBackgroundColor(Color.parseColor("#c0392b"));
            holder.mTitle.setTextColor(Color.parseColor("#FFFFFF"));
            holder.mDescription.setTextColor(Color.parseColor("#FFFFFF"));
        } else {
            holder.mCardView.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        holder.mTitle.setText(mOption.getmTitle());
        holder.mDescription.setText(mOption.getmDescription());
        Glide.with(mContext).load(mOption.getmIcon()).into(holder.mThumbnail);
        /**
         * Se detecta el evento click sobre el cardview
         * */
        holder.mCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (position) {
                    case 0:
                        Toast.makeText(mContext, "Enviar una alerta", Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        mContext.startActivity(new Intent(mContext, MainActivity.class));
                        ((Activity)mContext).finish();
                        break;
                    case 2:
                        mContext.startActivity(new Intent(mContext, SupportActivity.class));
                        break;
                    case 3:
                        SnackbarUtils.showSnackbar(mContext, mView, "Aun en desarrollo", R.color.colorAccent);
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
