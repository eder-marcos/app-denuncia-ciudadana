package com.ciudadana.denuncia.denunciaciudadana.wrappers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by (Sferea) Eder Marcos on 31/08/2017 at 11.
 * Este wrapper sirve para parsear el objeto que se recibe dentro de data
 * Por lo general se recibe un ArrayList de un modelo en especifico.
 * En ocaciones se recibe datos adicionales como la cantidad, paginas, etc.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class MessageResponseWrapper {

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("msg")
    private String mResponse;

    public MessageResponseWrapper() {
        /**
         * Constructor vacio
         * */
    }

    public String getmResponse() {
        return mResponse;
    }

    public void setmResponse(String mResponse) {
        this.mResponse = mResponse;
    }
}
