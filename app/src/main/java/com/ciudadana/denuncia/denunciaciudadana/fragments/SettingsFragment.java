package com.ciudadana.denuncia.denunciaciudadana.fragments;

import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;

/**
 * Created by Eder Marcos.
 */

public class SettingsFragment extends PreferenceFragment {

    /**
     * Variable estatica creada para identificar lo que se imprime en consola y debuguear de mejor manera
     * */

    /**
     * Variables
     * */

    /**
     * Funciones sobre escrita
     */

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//            addPreferencesFromResource(R.xml.pref_settings);

        PreferenceScreen root = getPreferenceManager().createPreferenceScreen(getActivity());
        root.setTitle("Configuracion");

        PreferenceCategory cat1 = new PreferenceCategory(root.getContext());
        cat1.setTitle("aaa");
        root.addPreference(cat1);

        ListPreference lang = new ListPreference(root.getContext());
        lang.setEntries(new CharSequence[]{"English", "Bangla"});
        lang.setEntryValues(new CharSequence[]{"en", "bn"});
        lang.setTitle("Choose Language");
        lang.setSummary("English");
        cat1.addPreference(lang);

        CheckBoxPreference check = new CheckBoxPreference(root.getContext());
        check.setTitle("Title Check");
        cat1.addPreference(check);

        PreferenceCategory cat2 = new PreferenceCategory(root.getContext());
        cat2.setTitle("cat2");
        root.addPreference(cat2);
        CheckBoxPreference check2 = new CheckBoxPreference(root.getContext());
        check2.setTitle("Title Check");
        cat2.addPreference(check2);

        PreferenceCategory cat3 = new PreferenceCategory(root.getContext());
        cat3.setTitle("cat3");
        root.addPreference(cat3);
        CheckBoxPreference check3 = new CheckBoxPreference(root.getContext());
        check3.setTitle("Title Check");
        cat3.addPreference(check3);

        PreferenceCategory cat4 = new PreferenceCategory(root.getContext());
        cat4.setTitle("cat4");
        root.addPreference(cat4);
        CheckBoxPreference check4 = new CheckBoxPreference(root.getContext());
        check4.setTitle("Title Check");
        cat4.addPreference(check4);

        PreferenceCategory cat5 = new PreferenceCategory(root.getContext());
        cat5.setTitle("cat5");
        root.addPreference(cat5);
        CheckBoxPreference check5 = new CheckBoxPreference(root.getContext());
        check5.setTitle("Title Check");
        cat5.addPreference(check5);

        PreferenceCategory cat6 = new PreferenceCategory(root.getContext());
        cat6.setTitle("cat6");
        root.addPreference(cat6);
        CheckBoxPreference check6 = new CheckBoxPreference(root.getContext());
        check6.setTitle("Title Check");
        cat6.addPreference(check6);

        PreferenceCategory cat7 = new PreferenceCategory(root.getContext());
        cat7.setTitle("cat7");
        root.addPreference(cat7);
        CheckBoxPreference check7 = new CheckBoxPreference(root.getContext());
        check7.setTitle("Title Check");
        cat7.addPreference(check7);

        PreferenceCategory cat8 = new PreferenceCategory(root.getContext());
        cat8.setTitle("cat8");
        root.addPreference(cat8);
        CheckBoxPreference check8 = new CheckBoxPreference(root.getContext());
        check8.setTitle("Title Check");
        cat8.addPreference(check8);

        PreferenceCategory cat9 = new PreferenceCategory(root.getContext());
        cat9.setTitle("cat9");
        root.addPreference(cat9);
        CheckBoxPreference check9 = new CheckBoxPreference(root.getContext());
        check9.setTitle("Title Check");
        cat9.addPreference(check9);

        setPreferenceScreen(root);
    }
}
