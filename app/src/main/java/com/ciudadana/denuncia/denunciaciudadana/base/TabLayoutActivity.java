package com.ciudadana.denuncia.denunciaciudadana.base;

/**
 * Created by Sferea.02 on 30/05/2017.
 */

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.config.Configuration;


/**
 * Permite habilitar las pestañas en la actividad que lo herede.
 * <p/>
 * Para habilitar las pestañas se deben realizar las siguientes tareas.
 * <p/>
 * 1. Heredar esta clase.
 * 2. Llamar al método setupTabLayout para inicializar las pestañas.
 * 3. Sobrescribir el método deployFragment con el comportamiento esperado para
 * cada pestaña.
 */
public abstract class TabLayoutActivity extends ToolbarActivity implements TabLayout.OnTabSelectedListener
{
//    public TabLayout mTabLayout;
    protected Fragment mTabFragment;
    protected int mFragmentId;
    public FragmentManager mFragmentManager;

    /**
     * Inicializa el TabLayout y establece los listeners.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
////        mTabLayout = (TabLayout) findViewById(R.id.tablayout);
////        mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
////        mTabLayout.addOnTabSelectedListener(this);
        mFragmentManager = getSupportFragmentManager();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
//        deployFragment(getSelectedTabPosition());
    }

    /**
     * Inicializar el TabLayout con los títulos recibidos en los parámetros.
     *
     * @param style      estilo que se le aplicará al TabLayout.
     * @param tabsTitles títulos que se mostrarán en las pestañas del TabLayout.
     */
//    public void setupTabLayout(int style, String... tabsTitles) {
//        TabLayout.Tab tab;
//        TextView tabTextView;
//        for(int i = 0; i < tabsTitles.length; i++)
//        {
////            tab = mTabLayout.newTab();
//            switch(style)
//            {
//                case 1:
//                    tabTextView = new TextView(this);
//                    tabTextView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//                    tabTextView.setGravity(Gravity.CENTER);
//                    tabTextView.setBackgroundColor(Color.WHITE);
//                    tabTextView.setId(android.R.id.text1);
//                    tabTextView.setTextColor(Color.BLACK);
//                    tab.setCustomView(tabTextView);
//                    tab.setText(tabsTitles[i]);
//                    break;
//                case 2: //Solo se muestra la primera letra de lo que envie
//                    tabTextView = new TextView(this);
//                    tabTextView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//                    tabTextView.setGravity(Gravity.CENTER);
//                    tabTextView.setBackgroundColor(Color.WHITE);
//                    tabTextView.setId(android.R.id.text1);
//                    tabTextView.setTextColor(Color.BLACK);
//                    tab.setCustomView(tabTextView);
//                    String tabTitle = "";
//                    if(tabsTitles[i] != null && !tabsTitles[i].isEmpty())
//                        tabTitle = tabsTitles[i].contains("HOY") ? tabsTitles[i] : tabsTitles[i].substring(0, 1);
//                    tab.setText(tabTitle);
//                    break;
//            }
////            mTabLayout.addTab(tab);
//        }
////        mTabLayout.setVisibility(View.VISIBLE);
//    }

//    public void removeTabLayout()
//    {
////        mTabLayout.removeAllTabs();
////        mTabLayout.setVisibility(View.GONE);
//    }

    /**
     * Se ejecuta cuando se selecciona alguna de las pestañas del
     * TabLayout.
     *
     * @param fragmentId id de la pestaña que se va a desplegar.
     */
    public void deployFragment(int fragmentId)
    {
    }

    /**
     * Avisa cuando se selecciona alguna de las pestañas del TabLayout.
     *
     * @param tab información de la pestaña seleccionada.
     */
    @Override
    public void onTabSelected(TabLayout.Tab tab)
    {
        if(tab.getCustomView() != null && tab.getCustomView() instanceof TextView)
        {
            tab.getCustomView().setBackgroundColor(Configuration.mToolbarColor1);
            ((TextView) tab.getCustomView()).setTextColor(Color.WHITE);
        }
//        deployFragment(mTabLayout.getSelectedTabPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab)
    {
        if(tab.getCustomView() != null && tab.getCustomView() instanceof TextView)
        {
            tab.getCustomView().setBackgroundColor(Color.WHITE);
            ((TextView) tab.getCustomView()).setTextColor(Configuration.mToolbarColor1);
        }
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab)
    {
    }

    /**
     * Permite establecer la visibilidad del TabLayout.
     *
     * @param visibility visibilidad del TabLayout.
     */
//    public void setTabLayoutVisibility(int visibility)
//    {
//        mTabLayout.setVisibility(visibility);
//    }

    /**
     * Obtiene la posición de la pestaña actual.
     *
     * @return posición de la pestaña actual.
     */
//    public int getSelectedTabPosition()
//    {
//        return mTabLayout.getSelectedTabPosition();
//    }
}