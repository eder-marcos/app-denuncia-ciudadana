package com.ciudadana.denuncia.denunciaciudadana.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.adapters.UserAdapter;
import com.ciudadana.denuncia.denunciaciudadana.models.UserModel;
import com.ciudadana.denuncia.denunciaciudadana.utils.ConvertTo;
import com.ciudadana.denuncia.denunciaciudadana.utils.GridSpacingItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eder Marcos.
 */

public class SupportActivity extends AppCompatActivity {

    /**
     * Variable para debug
     * */


    /**
     * Variables
     * */
    private UserAdapter mAdapter;
    private List<UserModel> mList;

    /**
     * UI elementos de android
     * */
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        /**
         * Localizacion de los elementos
         * */
        ((TextView) findViewById(R.id.tvToolbarTitleS)).setText("Soporte Técnico");
        mRecyclerView = findViewById(R.id.rvSupport);

        /**
         * Se inicializan las variables
         * */
        mList = new ArrayList<>();
        mAdapter = new UserAdapter(mList, this);

        UserModel mUser = new UserModel(
                "Eder",
                "Marcos",
                R.drawable.bg_eder,
                "skrap.marcos@gmail.com",
                "Programador",
                "Raster Studios",
                "Desarrollador Web y Móvil"
        );
        mList.add(mUser);

        mUser = new UserModel(
                "Carlos",
                "Hernandez",
                R.drawable.bg_carlos,
                "----",
                "Programador",
                "Raster Studios",
                "Desarrollador Web"
        );
        mList.add(mUser);
        mAdapter.notifyDataSetChanged();

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(1, ConvertTo.dpToPx(5, getResources()), true));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);
    }
}
