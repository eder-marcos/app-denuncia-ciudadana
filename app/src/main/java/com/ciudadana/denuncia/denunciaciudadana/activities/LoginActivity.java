package com.ciudadana.denuncia.denunciaciudadana.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.VolleyError;
import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.base.ReceiverActivity;
import com.ciudadana.denuncia.denunciaciudadana.models.LoginModel;
import com.ciudadana.denuncia.denunciaciudadana.models.Utils.Response;
import com.ciudadana.denuncia.denunciaciudadana.parser.Parser;
import com.ciudadana.denuncia.denunciaciudadana.utils.ConstantsUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.SnackbarUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.StoreSharedPreferencesUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.ValidationsUtils;
import com.ciudadana.denuncia.denunciaciudadana.web.receiver.Receiver;
import com.ciudadana.denuncia.denunciaciudadana.web.request.Request;
import com.ciudadana.denuncia.denunciaciudadana.wrappers.LoginWrapper;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Eder Marcos.
 * Actividad que muestra el login
 */

public class LoginActivity extends ReceiverActivity {

    /**
     * Variables
     * */
    public static final String TAG = ConstantsUtils.DEBUG;
    private LoginModel mModel;
    private String email;
    private StoreSharedPreferencesUtils mStore;

    /**
     * UI Elementos de Android
     * */
    private EditText mEmail;
    private EditText mPassword;
    private Button mLogin;
    private Button mSignup;
    private View mView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * Localizacion de los elementos
         * */
        mEmail = findViewById(R.id.email);
        mPassword = findViewById(R.id.password);
        mLogin = findViewById(R.id.email_sign_in_button);
        mSignup = findViewById(R.id.email_sign_up_button);
        mView = findViewById(R.id.main_content_login);

        /**
         * Se inicializan las variables
         * */
        mModel = new LoginModel();
        mLogin.setOnClickListener(letLogin);
        mSignup.setOnClickListener(letSignup);

        mStore = new StoreSharedPreferencesUtils(this);

        if (!ValidationsUtils.isEmpty(mStore.getData("email", ""))) {
            email = mStore.getData("email", "");
            mEmail.setText(email);
        }
    }

    /**
     * Variables del tipo OnclickListener
     * */
    View.OnClickListener letLogin = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            email = mEmail.getText().toString();
            String password = mPassword.getText().toString();

            if (!ValidationsUtils.isEmpty(email) &&
                    !ValidationsUtils.isEmpty(password)) {
                try {
                    HashMap<String, Object> mParams = new HashMap<>();
                    mParams.put("email", email);
                    mParams.put("password", password);
                    /**
                     * Se encargara de ejecutar la peticion y de recibir una respuesta del web service
                     * */
                    Receiver receiver = new Receiver(
                            LoginActivity.this,
                            Request.Method.JSON,
                            Parser.ParserType.RESPONSE,
                            LoginWrapper.class,
                            "Cargando"
                    );
                    receiver.execute(ConstantsUtils.BASE + "sesion/signIn/", "", new JSONObject(mParams).toString());
                    mRequestQueue.add(receiver);
                } catch (Exception e) {
                    Log.e(TAG, "Error al iniciar sesion: ", e);
                }
            }
        }
    };

    View.OnClickListener letSignup = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(LoginActivity.this, SignupActivity.class));
            finish();
        }
    };

    /**
     * Funcion sobre escrita
     * Infla el layout de esta actividad
     * */
    @Override
    public int getLayoutResource() {
        return R.layout.activity_login;
    }

    /**
     * Funcion sobre escrita
     * Respuesta del servidor
     * */
    @Override
    public void onResponse(Object response) {
        if (response instanceof Response) {
            if (((Response) response).getData() instanceof LoginWrapper) {
                mModel = new LoginModel(((LoginWrapper) ((Response) response).getData()).getmToken());
                if (((Response) response).getCodigo().getErrorCode().equals("200")
                        && ((Response) response).getCodigo().getMessageError().equals("Login exitoso")) {
                    String token = ((LoginWrapper) ((Response) response).getData()).getmToken();
                    Log.d(TAG, "Token: " + token);
                    mStore.saveData("Token", token);
                    mStore.saveData("email", email);
                    startActivity(new Intent(LoginActivity.this, ClassificationActivity.class));
                    finish();
                } else {
                    SnackbarUtils.showSnackbar(
                            LoginActivity.this,
                            mView,
                            ((Response) response).getCodigo().getMessageError(),
                            R.color.color_pomegranate
                    );
                }
            }
        }
    }

    @Override
    public void onError(VolleyError error) {
        super.onError(error);
        SnackbarUtils.showSnackbar(LoginActivity.this, mView, error.toString(), R.color.color_pomegranate);
    }
}