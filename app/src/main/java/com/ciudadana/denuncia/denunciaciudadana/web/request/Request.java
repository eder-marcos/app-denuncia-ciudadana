package com.ciudadana.denuncia.denunciaciudadana.web.request;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.ciudadana.denuncia.denunciaciudadana.interfaces.RequestListener;
import com.ciudadana.denuncia.denunciaciudadana.parser.Parser;
import com.ciudadana.denuncia.denunciaciudadana.web.VolleySingleton;

import org.json.JSONException;

import java.io.IOException;
import java.util.Map;

/**
 * Created by Sferea.02 on 30/05/2017.
 */

/**
 * Clase genérica que permite recibir, parsear y publicar la respuesta
 * a la clase que la llamó.
 */
public class Request implements Response.Listener<String>, Response.ErrorListener {
    public final String TAG = Request.class.getSimpleName();
    private Parser.ParserType mParserType;
    private RequestListener mListener;
    private Class mType;
    private StringRequest mStringRequest;
    private String mRootNode;

    public enum Method {
        POST, GET, PUT, JSON, DELETE
    }

    public interface ErrorCode {
        String TIMEOUT = "TIMEOUT";
        String SERVER = "ERRORENELSERVIDOR";
        String NO_CONECTION = "NOHAYINTERNET";
        String TOKEN_1 = "TOKENINVALIDO";
        String TOKEN_2 = "TOKENINVALIDADO";
    }

    public Request(Parser.ParserType parserType, Class type) {
        this.mType = type;
        mParserType = parserType;
    }

    public void makeRequest(String rootNode, Method method, String url, final Object params, RequestListener listener) {
        Log.d(TAG, "Request URL: " + url);
        if (params != null)
            Log.d(TAG, "Request Data: " + params.toString());
        this.mListener = listener;
        mRootNode = rootNode;
        switch (method) {
            case GET:
                mStringRequest = new StringRequest(com.android.volley.Request.Method.GET, url + (params == null ? "" : params), this, this);
                break;
            case POST:
                mStringRequest = new StringRequest(com.android.volley.Request.Method.POST, url, this, this) {
                    /**
                     * Publica los parametros que se enviarán al servicio web.
                     *
                     * @return parámetros en forma clave - valor.
                     *
                     * @throws AuthFailureError error de autenticación.
                     */
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        return (Map<String, String>) params;
                    }
                };
                break;
            case JSON:
                mStringRequest = new StringRequest(com.android.volley.Request.Method.POST, url, this, this) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }

                    @Override
                    public byte[] getBody() throws AuthFailureError {
                        return params == null ? null : ((String) params).getBytes();
                    }
                };
                break;
            case DELETE:
                mStringRequest = new StringRequest(com.android.volley.Request.Method.DELETE, url, this, this) {
                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8";
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        return (Map<String, String>) params;
                    }
                };
                break;
            case PUT:
                mStringRequest = new StringRequest(com.android.volley.Request.Method.PUT, url, this, this) {
                    /**
                     * Publica los parametros que se enviarán al servicio web.
                     *
                     * @return parámetros en forma clave - valor.
                     *
                     * @throws AuthFailureError error de autenticación.
                     */
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        return (Map<String, String>) params;
                    }
                };
                break;
        }
        setTimeout(4, 0, 1);
        VolleySingleton.getInstance().addToRequestQueue(mStringRequest);
    }

    public void setTimeout(int mult, int n_retry, int b_retry) {
        if (mStringRequest != null)
            mStringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * mult, n_retry, b_retry));
    }

    @Override
    public void onResponse(String stringResponse) {
        Log.d(TAG, "Response: " + stringResponse);
        try {
            Object item;
            item = Parser.parseJson(mRootNode, stringResponse, mParserType, mType);
            if (item instanceof com.ciudadana.denuncia.denunciaciudadana.models.Utils.Response) {
                com.ciudadana.denuncia.denunciaciudadana.models.Utils.Response response = (com.ciudadana.denuncia.denunciaciudadana.models.Utils.Response) item;
                if (!response.getCodigo().getErrorCode().contains("200") &&
                    !response.getCodigo().getErrorCode().contains("201") &&
                    !response.getCodigo().getErrorCode().contains("202")) {
                    if (mListener != null)
                        mListener.onError(new VolleyError(response.getCodigo().getMessageError()));
                    return;
                }
            }
            if (mListener != null)
                mListener.onResponse(item);
        } catch (JSONException e) {
            e.printStackTrace();
            if (mListener != null)
                mListener.onError(new VolleyError(new String("ErrorWrapper en el parseo")));
        } catch (IOException e) {
            e.printStackTrace();
            if (mListener != null)
                mListener.onError(new VolleyError(new String("ErrorWrapper")));
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (mListener != null)
            mListener.onError(error);
        error.printStackTrace();
    }

    public void cancelRequest() {
        if (mStringRequest != null)
            mStringRequest.cancel();
    }
}

