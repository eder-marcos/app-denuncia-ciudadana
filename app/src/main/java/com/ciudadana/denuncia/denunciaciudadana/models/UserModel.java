package com.ciudadana.denuncia.denunciaciudadana.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created Eder Marcos.
 * Un modelo es utilizado para representar los atributos del objeto que esta dentro de un array
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserModel implements Parcelable{

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("id")
    private String mId;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("name")
    private String mName;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("lastName")
    private String mLastName;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("phone")
    private String mPhone;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    private int mImageProfile;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("email")
    private String mEmail;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    private String mRole;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    private String mCompany;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    private String mJob;



    /**
     * Constructor vacio
     * */
    public UserModel() {
        /**
         * Funcion vacia
         * */
    }

    /**
     * Constructor vacio
     * */
    public UserModel(String mName, String mLastName, int mImageProfile, String mEmail, String mRole, String mCompany, String mJob) {
        this.mName = mName;
        this.mLastName = mLastName;
        this.mImageProfile = mImageProfile;
        this.mEmail = mEmail;
        this.mRole = mRole;
        this.mCompany = mCompany;
        this.mJob = mJob;
    }

    protected UserModel(Parcel in) {
        mId = in.readString();
        mName = in.readString();
        mLastName = in.readString();
        mPhone = in.readString();
        mImageProfile = in.readInt();
        mEmail = in.readString();
        mRole = in.readString();
        mCompany = in.readString();
        mJob = in.readString();
    }

    public static final Creator<UserModel> CREATOR = new Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel in) {
            return new UserModel(in);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mId);
        parcel.writeString(mName);
        parcel.writeString(mLastName);
        parcel.writeString(mPhone);
        parcel.writeInt(mImageProfile);
        parcel.writeString(mEmail);
        parcel.writeString(mRole);
        parcel.writeString(mCompany);
        parcel.writeString(mJob);
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmLastName() {
        return mLastName;
    }

    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    public String getmPhone() {
        return mPhone;
    }

    public void setmPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public int getmImageProfile() {
        return mImageProfile;
    }

    public void setmImageProfile(int mImageProfile) {
        this.mImageProfile = mImageProfile;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmRole() {
        return mRole;
    }

    public void setmRole(String mRole) {
        this.mRole = mRole;
    }

    public String getmCompany() {
        return mCompany;
    }

    public void setmCompany(String mCompany) {
        this.mCompany = mCompany;
    }

    public String getmJob() {
        return mJob;
    }

    public void setmJob(String mJob) {
        this.mJob = mJob;
    }
}
