package com.ciudadana.denuncia.denunciaciudadana.utils;

/**
 * Created by (Sferea) Eder Marcos on 14/08/2017 at 19.
 */

public class ConstantsUtils {

    private ConstantsUtils() {
        /**
         * Constructor
         * */
    }

    public static final String DEBUG = "[DEBUG] ";
    public static final String PREFERENCES = "PREFERENCES";
    public static final String TEMP_TOKEN_PREFERENCES = "TEMP_TOKEN_PREFERENCES";
    public static final String TOKEN_PREFERENCES = "TOKEN_PREFERENCES";
    public static final String ORDER_NUMBER = "ORDER_NUMBER";
    public static final String DESTINATION = "DESTINATION";
    public static final String REGION = "REGIONR";
    public static final String TOWN = "TOWN";
    public static final String CP = "CP";
    public static final String STREET = "STREET";

    public static final String SERVER = "http://192.168.1.184";
    public static final String BASE = SERVER + "/Dropbox/project/api-rest/public/";
}
