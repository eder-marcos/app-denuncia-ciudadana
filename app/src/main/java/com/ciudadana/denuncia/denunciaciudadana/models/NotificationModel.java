package com.ciudadana.denuncia.denunciaciudadana.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created Eder Marcos.
 * Un modelo es utilizado para representar los atributos del objeto que esta dentro de un array
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationModel {

    /**
     * Variables
     * */
    private int mImage;
    private String mTitle;
    private String mAction;
    private String mDescription;
    private String mHour;

    /**
     * Funcion
     * Constructor
     * */
    public NotificationModel() {
        /**
         * Funcion vacia
         * */
    }

    /**
     * Funcion
     * Constructor
     * */
    public NotificationModel(int mImage, String mTitle, String mAction, String mDescription, String mHour) {
        this.mImage = mImage;
        this.mTitle = mTitle;
        this.mAction = mAction;
        this.mDescription = mDescription;
        this.mHour = mHour;
    }

    /**
     * Metodos GET y SET
     * */
    public int getmImage() {
        return mImage;
    }

    public void setmImage(int mImage) {
        this.mImage = mImage;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmAction() {
        return mAction;
    }

    public void setmAction(String mAction) {
        this.mAction = mAction;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmHour() {
        return mHour;
    }

    public void setmHour(String mHour) {
        this.mHour = mHour;
    }
}
