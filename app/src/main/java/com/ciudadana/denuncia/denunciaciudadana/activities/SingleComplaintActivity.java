package com.ciudadana.denuncia.denunciaciudadana.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.base.ReceiverActivity;
import com.ciudadana.denuncia.denunciaciudadana.models.ComplaintModel;
import com.ciudadana.denuncia.denunciaciudadana.models.Utils.Response;
import com.ciudadana.denuncia.denunciaciudadana.parser.Parser;
import com.ciudadana.denuncia.denunciaciudadana.utils.ConstantsUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.SnackbarUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.StoreSharedPreferencesUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.ValidationsUtils;
import com.ciudadana.denuncia.denunciaciudadana.web.receiver.Receiver;
import com.ciudadana.denuncia.denunciaciudadana.web.request.Request;
import com.ciudadana.denuncia.denunciaciudadana.wrappers.ComplaintWrapper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by Eder Marcos.
 */

public class SingleComplaintActivity extends ReceiverActivity implements OnMapReadyCallback {

    /**
     * Variables
     * */
    private static final String TAG = ConstantsUtils.DEBUG;
    private String mLat;
    private String mLon;
    private StoreSharedPreferencesUtils mStore;
    private String mPage;

    /**
     * UI Elementos de android
     * */
    private ImageView mIconBack;
    private ImageView mImage;
    private ImageView mImageProfile;
    private ImageView mIcon1;
    private ImageView mIcon2;
    private ImageView mIcon3;
    private TextView mTitle;
    private TextView mHour;
    private TextView mDescription;
    private TextView mNameAdmin;
    private TextView mRoleAdmin;
    private TextView mStatusAdmin;
    private TextView mEmailAdmin;
    private GoogleMap mGoogleMap;
    private LatLng mLatOrigin;
    private View mView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * Localizacion de elementos
         * */
        mIconBack = findViewById(R.id.ivIcSingle);
        mImage = findViewById(R.id.ivImageSingle);
        mImageProfile = findViewById(R.id.ivProfileSingle);
        mIcon1 = findViewById(R.id.ivIcon1);
        mIcon2 = findViewById(R.id.ivIcon2);
        mIcon3 = findViewById(R.id.ivIcon3);
        mTitle = findViewById(R.id.tvTitleSingle);
        mHour = findViewById(R.id.tvHourSingle);
        mDescription = findViewById(R.id.tvDescSingle);
        mNameAdmin = findViewById(R.id.tvNameSingle);
        mRoleAdmin = findViewById(R.id.tvRoleS);
        mStatusAdmin = findViewById(R.id.tvStatus);
        mEmailAdmin = findViewById(R.id.tvEmailS);
        mView = findViewById(R.id.main_content_single);

        /**
         * Se inicializan las variables
         * */
        mStore = new StoreSharedPreferencesUtils(this);

        mLat = "20.095457";
        mLon = "-98.712690";
        mLatOrigin = new LatLng(
                Double.parseDouble(mLat),
                Double.parseDouble(mLon));
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.fMap);
        mapFragment.getMapAsync(this);
        mIconBack.setImageDrawable(getResources().getDrawable(R.drawable.ic_arrow_back));
        mIconBack.setColorFilter(Color.parseColor("#ffffff"));
        mIcon1.setImageDrawable(getResources().getDrawable(R.drawable.ic_verified_user));
        mIcon1.setColorFilter(Color.parseColor("#959599"));
        mIcon2.setImageDrawable(getResources().getDrawable(R.drawable.ic_status));
        mIcon2.setColorFilter(Color.parseColor("#959599"));
        mIcon3.setImageDrawable(getResources().getDrawable(R.drawable.ic_email));
        mIcon3.setColorFilter(Color.parseColor("#959599"));

        mIconBack.setOnClickListener(letBackActivity);

        mPage = getIntent().getStringExtra("IdComplaint");
        /**
         * Peticion al servidor
         * */
        getComplaint();
    }

    /**
     * Variable
     * Variable del tipo onClickListener
     * */
    View.OnClickListener letBackActivity = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    /**
     * Funcion sobre escrita
     * Cuando el mapa esta listo
     * */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mGoogleMap = googleMap;
        mGoogleMap.addMarker(new MarkerOptions().position(mLatOrigin));
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatOrigin, 15));
    }

    /**
     * Funcion sobre escrita
     * Layout de la actividad
     * */
    @Override
    public int getLayoutResource() {
        return R.layout.activity_single_complaint;
    }

    private void getComplaint() {
        if (!ValidationsUtils.isEmpty(mStore.getData("Token", ""))) {
            try {
                Receiver receiver = new Receiver(
                        SingleComplaintActivity.this,
                        Request.Method.GET,
                        Parser.ParserType.RESPONSE,
                        ComplaintWrapper.class,
                        "Cargando"
                );
                receiver.execute(ConstantsUtils.BASE + "complaint/get/" + mPage + "/?token=" + mStore.getData("Token", ""), "");
                mRequestQueue.add(receiver);
            } catch (Exception e) {
                Log.e(TAG, "Error al obtener denuncias: ", e);
            }
        } else {
            SnackbarUtils.showSnackbar(this, mView, "Token requerido", R.color.color_pomegranate);
        }
    }

    /**
     * Funcion sobre escrita
     * Respuesta del servidor
     * */
    @Override
    public void onResponse(Object response) {
        if (response instanceof Response) {
            if (((Response) response).getData() instanceof ComplaintWrapper) {
                ComplaintModel mModel = ((ComplaintWrapper) ((Response) response).getData()).getmComplaint();
                mTitle.setText(mModel.getmTitle());
                mHour.setText(mModel.getmCreatedAt().split(" ")[1]);
                mDescription.setText(mModel.getmDescription());
                mNameAdmin.setText("----");
                mRoleAdmin.setText("----");
                mStatusAdmin.setText("----");
                mEmailAdmin.setText("----");
            }
        }
    }

    @Override
    public void onError(VolleyError error) {
        super.onError(error);
        Log.d(TAG, "onError");
        SnackbarUtils.showSnackbar(this, mView, error.toString(), R.color.color_pomegranate);
    }

}
