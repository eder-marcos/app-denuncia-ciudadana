package com.ciudadana.denuncia.denunciaciudadana.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import java.io.FileOutputStream;

/**
 * Created by (Sferea) Eder Marcos on 08/09/2017 at 13.
 */

public class SignatureUtils extends View {

    /**
     * Esta variable siempre es creada en todos los archivos para que cuando se realice DEBUG, se pueda filtrar en la consola con el valor de esta variable
     * */
    private static final String TAG = ConstantsUtils.DEBUG;

    /**
     * Declaracion de elementos del tipo view u otros
     * */
    private Paint mPaint = new Paint();
    private Path mPath = new Path();
    private final RectF mRectF = new RectF();

    /**
     * Declaracion de puras variables
     * */
    private float lastTouchX;
    private float lastTouchY;
    private static final float STROKE_WIDTH = 2f;
    private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;

    /**
     * Constructor
     * */
    public SignatureUtils(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeWidth(STROKE_WIDTH);
    }

    /**
     * Funcion de Utileria
     * */
    public void save(View v, String StoredPath, Bitmap bitmap, LinearLayout mContent) {
        if (bitmap == null) {
            bitmap = Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);
        }
        Canvas canvas = new Canvas(bitmap);
        try {
            FileOutputStream mFileOutStream = new FileOutputStream(StoredPath);
            v.draw(canvas);
            /**
             * Convierte el archivo de salida como imagen en formato .png
             * */
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
            mFileOutStream.flush();
            mFileOutStream.close();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    /**
     * Funcion de Utileria
     * */
    public void clear() {
        mPath.reset();
        invalidate();
    }

    /**
     * Funcion de Utileria
     * */
    private void expandDirtyRect(float historicalX, float historicalY) {
        if (historicalX < mRectF.left) {
            mRectF.left = historicalX;
        } else if (historicalX > mRectF.right) {
            mRectF.right = historicalX;
        }

        if (historicalY < mRectF.top) {
            mRectF.top = historicalY;
        } else if (historicalY > mRectF.bottom) {
            mRectF.bottom = historicalY;
        }
    }

    private void resetDirtyRect(float eventX, float eventY) {
        mRectF.left = Math.min(lastTouchX, eventX);
        mRectF.right = Math.max(lastTouchX, eventX);
        mRectF.top = Math.min(lastTouchY, eventY);
        mRectF.bottom = Math.max(lastTouchY, eventY);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPath(mPath, mPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float eventX = event.getX();
        float eventY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mPath.moveTo(eventX, eventY);
                lastTouchX = eventX;
                lastTouchY = eventY;
                return true;

            case MotionEvent.ACTION_MOVE:
            case MotionEvent.ACTION_UP:
                resetDirtyRect(eventX, eventY);
                int historySize = event.getHistorySize();
                for (int i = 0; i < historySize; i++) {
                    float historicalX = event.getHistoricalX(i);
                    float historicalY = event.getHistoricalY(i);
                    expandDirtyRect(historicalX, historicalY);
                    mPath.lineTo(historicalX, historicalY);
                }
                mPath.lineTo(eventX, eventY);
                break;

            default:
                return false;
        }

        invalidate((int) (mRectF.left - HALF_STROKE_WIDTH),
                (int) (mRectF.top - HALF_STROKE_WIDTH),
                (int) (mRectF.right + HALF_STROKE_WIDTH),
                (int) (mRectF.bottom + HALF_STROKE_WIDTH));

        lastTouchX = eventX;
        lastTouchY = eventY;
        return true;
    }
}
