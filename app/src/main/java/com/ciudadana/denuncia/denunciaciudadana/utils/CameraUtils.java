package com.ciudadana.denuncia.denunciaciudadana.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.widget.ImageView;

import java.io.File;

/**
 * Created by (Sferea) Eder Marcos on 07/09/2017 at 13.
 */

public class CameraUtils {

    /**
     * Constructor
     * */
    private CameraUtils() {
        /**
         * Constructor vacio
         * */
    }

    /**
     * Funcion de Utileria
     * Retorna un Bitmap que ha sido almecenada en el telefono
     * @param path Direccion donde esta almacenada en el dispositivo
     * @param reqWidth Ancho que tendra la imagen
     * @param reqHeight Alto que tendra la imagen
     * */
    public static Bitmap decodeSampledBitmapFromFile(String path, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        final int height = options.outHeight;
        final int width = options.outWidth;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        int inSampleSize = 1;
        if (height > reqHeight) {
            inSampleSize = Math.round((float)height / (float)reqHeight);
        }
        int expectedWidth = width / inSampleSize;
        if (expectedWidth > reqWidth) {
            inSampleSize = Math.round((float)width / (float)reqWidth);
        }
        options.inSampleSize = inSampleSize;
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    /**
     * Funcion de Utileria
     * Peticion para poder acceder a la camara
     * @param activity Actividad en la que sera ejecutado
     * @param requestCode Codigo del request para identificar que ImageView hizo la peticion
     * */
    public static void cameraRequest(Activity activity, int requestCode) {
        Intent mIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + "image.jpg");
        mIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
        activity.startActivityForResult(mIntent, requestCode);
    }

    /**
     * Funcion de Utileria
     * Comprueba si un ImageView tiene seteada una imagen o esta vacia
     * @param view ImageView en el que esta almacenado la imagen
     * */
    public static boolean hasImage(@NonNull ImageView view) {
        Drawable drawable = view.getDrawable();
        boolean hasImage = (drawable != null);

        if (hasImage && (drawable instanceof BitmapDrawable)) {
            hasImage = ((BitmapDrawable)drawable).getBitmap() != null;
        }

        return hasImage;
    }

    /**
     * Funcion de Utileria
     * Comprueba si un ImageView tiene seteada una imagen o esta vacia
     * @param view ImageView en el que esta almacenado la imagen
     * @param id Id del la imagen que esta seteada por default
     * */
    public static boolean hasImage(@NonNull ImageView view, @NonNull int id, @NonNull Activity activity) {
        return view.getDrawable().getConstantState().equals(ActivityCompat.getDrawable(activity, id).getConstantState());
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }
}
