package com.ciudadana.denuncia.denunciaciudadana.utils;



/**
 * Created by (Sferea) Eder Marcos on 22/08/2017 at 12.
 * Clase de Utileria
 */

public class ListUtils {

    /**
     * Clase de Utileria
     * Convierte un ArrayList en una lista
     * @param data: ArrayList del tipo de dato RegionModel
     * @return List: Retorna una lista obtenida del ArrayList
     * */
//    public static List<String> convertToListRegions(ArrayList<RegionModel> data) {
//        List<String> responseList = new ArrayList<>();
//        if (data != null) {
//            for (int i = 0; i < data.size(); i++) {
//                responseList.add(data.get(i).getmRegion());
//            }
//        }
//        return responseList;
//    }

    /**
     * Clase de Utileria
     * Convierte un ArrayList en una lista
     * @param data: ArrayList del tipo de dato TownModel
     * @return List: Retorna una lista obtenida del ArrayList
     * */
//    public static List<String> convertToListTowns(ArrayList<TownModel> data) {
//        List<String> responseList = new ArrayList<>();
//        if (data != null) {
//            for (int i = 0; i < data.size(); i++) {
//                responseList.add(data.get(i).getmTown());
//            }
//        }
//        return responseList;
//    }
}
