package com.ciudadana.denuncia.denunciaciudadana.models;

/**
 * Created by Eder Marcos.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EvidenceModel implements Parcelable {

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("id")
    private String mId;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("complaint_id")
    private String mComplaintId;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("imageSource")
    private String mImageSource;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("videoSource")
    private String mVideoSource;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("createdAt")
    private String mCreatedAt;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("updatedAt")
    private String mUpdatedAt;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("removed")
    private boolean mRemoved;

    /**
     * Funcion
     * Constructor vacio
     * */
    public EvidenceModel() {
        /**
         * Funcion vacia
         * */
    }


    protected EvidenceModel(Parcel in) {
        mId = in.readString();
        mComplaintId = in.readString();
        mImageSource = in.readString();
        mVideoSource = in.readString();
        mCreatedAt = in.readString();
        mUpdatedAt = in.readString();
        mRemoved = in.readByte() != 0;
    }

    public static final Creator<EvidenceModel> CREATOR = new Creator<EvidenceModel>() {
        @Override
        public EvidenceModel createFromParcel(Parcel in) {
            return new EvidenceModel(in);
        }

        @Override
        public EvidenceModel[] newArray(int size) {
            return new EvidenceModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mId);
        parcel.writeString(mComplaintId);
        parcel.writeString(mImageSource);
        parcel.writeString(mVideoSource);
        parcel.writeString(mCreatedAt);
        parcel.writeString(mUpdatedAt);
        parcel.writeByte((byte) (mRemoved ? 1 : 0));
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmComplaintId() {
        return mComplaintId;
    }

    public void setmComplaintId(String mComplaintId) {
        this.mComplaintId = mComplaintId;
    }

    public String getmImageSource() {
        return mImageSource;
    }

    public void setmImageSource(String mImageSource) {
        this.mImageSource = mImageSource;
    }

    public String getmVideoSource() {
        return mVideoSource;
    }

    public void setmVideoSource(String mVideoSource) {
        this.mVideoSource = mVideoSource;
    }

    public String getmCreatedAt() {
        return mCreatedAt;
    }

    public void setmCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public String getmUpdatedAt() {
        return mUpdatedAt;
    }

    public void setmUpdatedAt(String mUpdatedAt) {
        this.mUpdatedAt = mUpdatedAt;
    }

    public boolean ismRemoved() {
        return mRemoved;
    }

    public void setmRemoved(boolean mRemoved) {
        this.mRemoved = mRemoved;
    }
}
