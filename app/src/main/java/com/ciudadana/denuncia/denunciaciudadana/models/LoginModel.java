package com.ciudadana.denuncia.denunciaciudadana.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Eder Marcos.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginModel implements Parcelable {

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("token")
    private String mToken;


    /**
     * Constructor
     * */
    public LoginModel() {
    }

    /**
     * Constructor
     * */
    public LoginModel(String mToken) {
        this.mToken = mToken;
    }

    protected LoginModel(Parcel in) {
        mToken = in.readString();
    }

    public static final Creator<LoginModel> CREATOR = new Creator<LoginModel>() {
        @Override
        public LoginModel createFromParcel(Parcel in) {
            return new LoginModel(in);
        }

        @Override
        public LoginModel[] newArray(int size) {
            return new LoginModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mToken);
    }

    public String getmToken() {
        return mToken;
    }

    public void setmToken(String mToken) {
        this.mToken = mToken;
    }
}
