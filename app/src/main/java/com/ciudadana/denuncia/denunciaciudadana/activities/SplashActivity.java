package com.ciudadana.denuncia.denunciaciudadana.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.config.Configuration;
import com.ciudadana.denuncia.denunciaciudadana.utils.ConstantsUtils;

/**
 * Created by Eder Marcos.
 * Actividad que muestra el splash inicial de la aplicacion
 */

public class SplashActivity extends AppCompatActivity {

    /**
     * Esta variable siempre es creada en todos los archivos para que cuando se realice DEBUG, se pueda filtrar en la consola con el valor de esta variable
     * */
    private static final String TAG = ConstantsUtils.DEBUG;
    private static int mSplashTime = 2000;

    /**
     * Declaracion de elementos del tipo view u otros
     * */
    private View mContentView;

    /**
     * Declaracion de puras variables
     * */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * Carga el layout correspondiente
         * */
        setContentView(R.layout.activity_splash);
        mContentView = findViewById(R.id.fullscreen_content);
        /**
         * Oculta la barra de navegacion y el action bar
         * */
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        /**
         * Tiempo de espera en la que se muestra el splash
         * */
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /**
                 * Despues de cierto tiempo, manda a llamar la funcion que carga la siguiente pantalla
                 * */
                Intent mIntent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(mIntent);
                finish();
            }
        }, mSplashTime);
    }

    /**
     * Al presionar el boton de Regresar, la aplicacion se cierra
     * Solo es valida cuando esta activa esta actividad
     * */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
