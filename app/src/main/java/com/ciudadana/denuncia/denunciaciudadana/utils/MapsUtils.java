package com.ciudadana.denuncia.denunciaciudadana.utils;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

/**
 * Created by Skrapz on 21/09/2017.
 * Clase de Utileria
 * Tiene Funciones de ayuda para manejar mejor los mapas
 */

public class MapsUtils {

    /**
     * Esta variable siempre es creada en todos los archivos para que cuando se realice DEBUG, se pueda filtrar en la consola con el valor de esta variable
     * */
    private static final String TAG = ConstantsUtils.DEBUG;

    /**
     * Clase de Utileria
     * @param strAddress: Direccion de la ubicacion de la que se quiere obtener la latitud y longitud
     * @param activity: Actividad en la que se ejecuta
     * @return Retorna una latitud y longitud
     * */
    public static LatLng getLocationFromAddress(String strAddress, Activity activity) {
        Geocoder mGeocoder = new Geocoder(activity);
        List<Address> mListAddress;
        LatLng mLatLng = null;
        try {
            mListAddress = mGeocoder.getFromLocationName(strAddress, 5);
            if (mListAddress == null)
                return null;
            Address mLocation = mListAddress.get(0);
            mLocation.getLatitude();
            mLocation.getLongitude();
            mLatLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
        } catch (IOException e) {
            Log.e(TAG, "getLocationFromAddress: ", e);
        }
        return mLatLng;
    }
}
