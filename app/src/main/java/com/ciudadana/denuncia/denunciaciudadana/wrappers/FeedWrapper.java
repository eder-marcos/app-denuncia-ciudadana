package com.ciudadana.denuncia.denunciaciudadana.wrappers;

import com.ciudadana.denuncia.denunciaciudadana.models.ComplaintModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by Eder Marcos.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedWrapper {

    /**
     * Este es l nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * Para cuando responda la peticion de tipo de transporte
     * */
    @JsonProperty("complaints")
    ArrayList<ComplaintModel> mList;

    /**
     * Constructor
     * */
    public FeedWrapper() {
        /**
         * Funcion vacia
         * */
    }

    public ArrayList<ComplaintModel> getmList() {
        return mList;
    }

    public void setmList(ArrayList<ComplaintModel> mList) {
        this.mList = mList;
    }
}
