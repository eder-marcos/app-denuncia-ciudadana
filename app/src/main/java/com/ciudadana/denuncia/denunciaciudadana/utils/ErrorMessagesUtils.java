package com.ciudadana.denuncia.denunciaciudadana.utils;

import com.android.volley.VolleyError;

/**
 * Created by (Sferea) Eder Marcos on 07/09/2017 at 16.
 * Clase de utileria
 * Dependiendo del tipo de mensaje recibido, se mostrar un mensaje personalizado para mostrar al usuario
 *
 */

public class ErrorMessagesUtils {

    /**
     * Constructor
     * */
    public ErrorMessagesUtils() {
        /**
         * Constructor vacio
         * */
    }

    /**
     * Funcion de Utileria
     * @param err: Recibe un error del tipo VolleyError, que tiene informacion del error en la peticion
     * @return Retorna un mensaje personalizado para que sea mostrado al usuario
     * */
    public static String getMessage(VolleyError err) {
        String msg = null;
        if (err != null) {
            /**
             * Verificacion si el mensaje de error es no es nulo
             * */
            if (!ValidationsUtils.isEmpty(err.toString())) {
                /**
                 * Se realiza un split al mensaje de error, ya que originalmente viene de la siguiente manera
                 * com.android.volley.VolleyError: MENSAJE
                 * */
                String response = err.toString().split(" ")[1];
                if (!ValidationsUtils.isEmpty(response)) {
                    msg = getCase(response);
                }
            }
        }
        return msg;
    }

    /**
     * Funcion de Utileria
     * @param err: Recibe un id de un tipo de error, especialmente para mostrar errores personalizados que no sean de una peticion
     * @return Retorna un mensaje dependiendo el id del codigo de error
     * */
    public static String getMessage(String err) {
        return getCase(err);
    }

    /**
     * Funcion de Utileria
     * @param mData: Codigo (id) de identificacion de cada mensaje de error
     * @return Retorna un mensaje dependiendo el id del codigo de error
     * */
    private static String getCase(String mData) {
        switch (mData) {
            case "SINCONTENIDO":
                return "Sin contenido";
            case "CONTENIDOPARCIAL":
                return "Contenido parcial";
            case "BADREQUEST":
                return "Bad request";
            case "SINAUTORIZACION":
                return "Sin autorizacion";
            case "SEREQUIERESESIONACTIVA":
                return "Se requiere sesion activa";
            case "NOENCONTRADO":
                return "No encontrado";
            case "CONFLICT":
                return "Peticion procesada con errores";
            case "INTERNALSERVERERROR":
                return "Error interno";
            case "NOTIMPLEMENTED":
                return "No implementado";
            case "SEREQUIERESESIÓNACTIVA":
                return "Se requiere sesion activa";
            case "CODIGOBARRASINVALIDA":
                return "Codigo de barras no válido";
            case "REQUIRED":
                return "Todos los datos son necesarios";
            default:
                return "Hubo un error inesperado";
        }
    }
}
