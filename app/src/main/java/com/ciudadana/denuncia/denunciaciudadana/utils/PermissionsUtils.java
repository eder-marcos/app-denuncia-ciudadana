package com.ciudadana.denuncia.denunciaciudadana.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;

/**
 * Created by Eder Marcos.
 */

public class PermissionsUtils {
    public static boolean check(Activity activity, String mPermission) {
        return ActivityCompat.checkSelfPermission(activity, mPermission) != PackageManager.PERMISSION_GRANTED;
    }

    public static boolean rationale(Activity activity, String mPermission) {
        return ActivityCompat.shouldShowRequestPermissionRationale(activity, mPermission);
    }

    public static void alertDialog(
            final Activity activity,
            final String[] mPermission,
            final int mPermisionCode,
            String mTitle,
            String mMessage,
            String mPositive,
            String mNegative) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(mTitle);
        builder.setMessage(mMessage);
        builder.setPositiveButton(mPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                ActivityCompat.requestPermissions(activity, mPermission, mPermisionCode);
            }
        });
        builder.setNegativeButton(mNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public static boolean alertDialogSettings(
            final Activity activity,
            final String[] mPermission,
            final int mPermisionCode,
            String mTitle,
            String mMessage,
            String mPositive,
            String mNegative) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(mTitle);
        builder.setMessage(mMessage);
        builder.setPositiveButton(mPositive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", activity.getPackageName(), null);
                intent.setData(uri);
                activity.startActivityForResult(intent, 2);
            }
        });
        builder.setNegativeButton(mNegative, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
        return true;
    }
}
