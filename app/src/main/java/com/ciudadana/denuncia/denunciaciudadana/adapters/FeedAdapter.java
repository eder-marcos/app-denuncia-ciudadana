package com.ciudadana.denuncia.denunciaciudadana.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.activities.SingleComplaintActivity;
import com.ciudadana.denuncia.denunciaciudadana.models.ComplaintModel;

import java.util.List;

import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * Created by Eder Marcos.
 */

public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.MyViewHolder> {

    /**
     * Variable estatica creada para identificar lo que se imprime en consola y debuguear de mejor manera
     * */

    /**
     * Variables
     * */
    private List<ComplaintModel> mList;

    /**
     * Declaracion de elementos que pertenecen a Android
     * */
    private Context mContext;

    /**
     * Clase
     * Clase MyViewHolder
     * */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitle;
        public TextView mDescription;
        public ImageView mIcon;
        public ImageView mOverflow;
        public CardView mContainer;

        public MyViewHolder(View view) {
            super(view);
            mIcon = view.findViewById(R.id.ivIconC);
            mTitle = view.findViewById(R.id.tvTitleC);
            mDescription = view.findViewById(R.id.tvDescriptionC);
            mContainer = view.findViewById(R.id.cvTypeComplaint);
            mOverflow = view.findViewById(R.id.ivOverflow);
        }
    }

    /**
     * Constructor
     * */
    public FeedAdapter() {
        /**
         * Funcion vacia
         * */
    }

    /**
     * Constructor
     * */
    public FeedAdapter(List<ComplaintModel> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public FeedAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_feed_card, parent, false);

        return new FeedAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FeedAdapter.MyViewHolder holder, final int position) {
        final ComplaintModel mItem = mList.get(position);
        /**
         * Se establecen los valores correspondientes
         * */
        holder.mTitle.setText(mItem.getmTitle());
        holder.mDescription.setText(mItem.getmDescription());
        Glide.with(mContext)
                .load(R.drawable.bg_1_3)
                .into(holder.mIcon);

        /**
         * Se detecta el evento click sobre el cardview
         * */
        holder.mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mContext, SingleComplaintActivity.class);
                mIntent.putExtra("IdComplaint", mItem.getmId());
                (mContext).startActivity(mIntent);
            }
        });
        holder.mOverflow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(holder.mOverflow);
            }
        });
    }

    /**
     * Funcion
     * Muestra un popup al presionar la imagen con los 3 dots
     * */
    private void showPopupMenu(View view) {
        // inflate menu
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_feed, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
        popup.show();
    }

    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        public MyMenuItemClickListener() {
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_1:
                    Toast.makeText(mContext, "Ocultar", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.action_2:
                    Toast.makeText(mContext, "Accion 2", Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}