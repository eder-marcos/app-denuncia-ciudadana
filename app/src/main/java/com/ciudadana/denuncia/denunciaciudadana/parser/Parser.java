package com.ciudadana.denuncia.denunciaciudadana.parser;

import android.util.Log;

import com.ciudadana.denuncia.denunciaciudadana.models.Utils.Response;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

/**
 * Created by Sferea.02 on 30/05/2017.
 */

public class Parser
{
    public enum ParserType
    {
        RESPONSE, OBJECT, ARRAY, TEXT
    }

    public static <T> Object parseJson(String node, String json, ParserType parserType, Class<T> type) throws JSONException, IOException
    {
        ObjectMapper objectMapper = new ObjectMapper();
        Object response;
        JSONObject jsonRequest;
        String request;
        try
        {
            jsonRequest = new JSONObject(json);
            request = (node != null) && (!node.isEmpty()) ? jsonRequest.getString(node) : json;
        }
        catch(Exception e)
        {
            request = json;
        }
        switch(parserType)
        {
            case OBJECT:
                response = objectMapper.readValue(request, type);
                break;
            case ARRAY:
                JavaType listType = objectMapper.getTypeFactory().constructCollectionType(List.class, type);
                response = objectMapper.readValue(request, listType);
                break;
            case RESPONSE:
                response = objectMapper.readValue(request, Response.class);
                JSONObject jsonObject = new JSONObject(json);
                Log.i("Data",json);
                Object data = objectMapper.readValue(jsonObject.getString("data"), type);
                ((Response) response).setData(data);
                break;
            case TEXT:
                response = json;
                break;
            default:
                response = null;
                break;
        }
        return response;
    }

    /**
     * Convierte un objeto a json.
     *
     * @param object objeto que se convertira a json.
     * @return cadena que contiene los datos del objeto.
     * @throws JsonProcessingException error al convertir el objeto a json.
     */
    public static String serialize(Object object) throws JsonProcessingException
    {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }
}