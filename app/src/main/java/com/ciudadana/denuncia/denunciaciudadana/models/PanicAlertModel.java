package com.ciudadana.denuncia.denunciaciudadana.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Eder Marcos.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PanicAlertModel implements Parcelable {

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("id")
    private String mId;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("longitude")
    private String mLongitude;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("latitude")
    private String mLatitude;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("createdAt")
    private String mCreatedAt;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("viewed")
    private boolean mViewed;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("messages_id")
    private String mMessagesId;

    /**
     * Funcion
     * Constructor vacio
     * */
    public PanicAlertModel() {
        /**
         * Funcion vacia
         * */
    }

    protected PanicAlertModel(Parcel in) {
        mId = in.readString();
        mLongitude = in.readString();
        mLatitude = in.readString();
        mCreatedAt = in.readString();
        mViewed = in.readByte() != 0;
        mMessagesId = in.readString();
    }

    public static final Creator<PanicAlertModel> CREATOR = new Creator<PanicAlertModel>() {
        @Override
        public PanicAlertModel createFromParcel(Parcel in) {
            return new PanicAlertModel(in);
        }

        @Override
        public PanicAlertModel[] newArray(int size) {
            return new PanicAlertModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mId);
        parcel.writeString(mLongitude);
        parcel.writeString(mLatitude);
        parcel.writeString(mCreatedAt);
        parcel.writeByte((byte) (mViewed ? 1 : 0));
        parcel.writeString(mMessagesId);
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(String mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(String mLatitude) {
        this.mLatitude = mLatitude;
    }

    public String getmCreatedAt() {
        return mCreatedAt;
    }

    public void setmCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public boolean ismViewed() {
        return mViewed;
    }

    public void setmViewed(boolean mViewed) {
        this.mViewed = mViewed;
    }

    public String getmMessagesId() {
        return mMessagesId;
    }

    public void setmMessagesId(String mMessagesId) {
        this.mMessagesId = mMessagesId;
    }
}
