package com.ciudadana.denuncia.denunciaciudadana.activities;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.HashMap;

import com.android.volley.VolleyError;
import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.base.ReceiverActivity;
import com.ciudadana.denuncia.denunciaciudadana.models.LoginModel;
import com.ciudadana.denuncia.denunciaciudadana.models.Utils.Response;
import com.ciudadana.denuncia.denunciaciudadana.parser.Parser;
import com.ciudadana.denuncia.denunciaciudadana.utils.ConstantsUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.SnackbarUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.StoreSharedPreferencesUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.ValidationsUtils;
import com.ciudadana.denuncia.denunciaciudadana.web.receiver.Receiver;
import com.ciudadana.denuncia.denunciaciudadana.web.request.Request;
import com.ciudadana.denuncia.denunciaciudadana.wrappers.ResponseSetWrapper;

import org.json.JSONObject;

/**
 * A login screen that offers login via email/password.
 */

public class SignupActivity extends ReceiverActivity {

    /**
     * Variables
     * */
    public static final String TAG = ConstantsUtils.DEBUG;

    /**
     * UI Elementos de Android
     * */
    private EditText mEmail;
    private EditText mName;
    private EditText mLastName;
    private EditText mPassword;
    private Button mLogin;
    private Button mSignup;
    private View mView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /**
         * Localizacion de los elementos
         * */
        mEmail = findViewById(R.id.email);
        mName = findViewById(R.id.etName);
        mLastName = findViewById(R.id.etLastName);
        mPassword = findViewById(R.id.password);
        mLogin = findViewById(R.id.btnLogin);
        mSignup = findViewById(R.id.btnSignup);
        mView = findViewById(R.id.main_content_signup);

        /**
         * Se inicializan las variables
         * */
        mLogin.setOnClickListener(letLogin);
        mSignup.setOnClickListener(letSignup);
    }

    /**
     * Variables del tipo OnclickListener
     * */
    View.OnClickListener letSignup = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String name = mName.getText().toString().trim();
            String lastName = mLastName.getText().toString().trim();
            String email = mEmail.getText().toString().toLowerCase().trim();
            String password = mPassword.getText().toString();

            if (!ValidationsUtils.isEmpty(email) &&
                !ValidationsUtils.isEmpty(name) &&
                !ValidationsUtils.isEmpty(lastName) &&
                !ValidationsUtils.isEmpty(password)) {
                try {
                    HashMap<String, Object> mParams = new HashMap<>();
                    mParams.put("name", name);
                    mParams.put("lastName", lastName);
                    mParams.put("email", email);
                    mParams.put("password", password);
                    /**
                     * Se encargara de ejecutar la peticion y de recibir una respuesta del web service
                     * */
                    Receiver receiver = new Receiver(
                            SignupActivity.this,
                            Request.Method.JSON,
                            Parser.ParserType.RESPONSE,
                            ResponseSetWrapper.class,
                            "Cargando"
                    );
                    receiver.execute(ConstantsUtils.BASE + "sesion/signUp/", "", new JSONObject(mParams).toString());
                    mRequestQueue.add(receiver);
                } catch (Exception e) {
                    Log.e(TAG, "Error al hacer el registro: ", e);
                }
            }
        }
    };

    View.OnClickListener letLogin = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(SignupActivity.this, LoginActivity.class));
            finish();
        }
    };

    /**
     * Funcion sobre escrita
     * Infla el layout de esta actividad
     * */
    @Override
    public int getLayoutResource() {
        return R.layout.activity_signup;
    }

    /**
     * Funcion sobre escrita
     * Respuesta del servidor
     * */
    @Override
    public void onResponse(Object response) {
        if (response instanceof Response) {
            if (((Response) response).getData() instanceof ResponseSetWrapper) {
                if (((Response) response).getCodigo().getErrorCode().equals("200")
                        && ((Response) response).getCodigo().getMessageError().equals("Registro agregado")) {
                    startActivity(new Intent(SignupActivity.this, LoginActivity.class));
                    finish();
                } else {
                    SnackbarUtils.showSnackbar(
                            SignupActivity.this,
                            mView,
                            ((Response) response).getCodigo().getMessageError(),
                            R.color.color_pomegranate
                    );
                }
            }
        }
    }

    @Override
    public void onError(VolleyError error) {
        super.onError(error);
        SnackbarUtils.showSnackbar(SignupActivity.this, mView, error.toString(), R.color.color_pomegranate);
    }
}