package com.ciudadana.denuncia.denunciaciudadana.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Eder Marcos.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class AlertsModel implements Parcelable {

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("id")
    private String mId;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("title")
    private String mTitle;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("message")
    private String mMessage;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("action")
    private String mAction;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("viewed")
    private boolean mViewed;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("complaint_id")
    private String mComplaintId;

    /**
     * Funcion
     * Constructor vacio
     * */
    public AlertsModel() {
        /**
         * Funcion vacia
         * */
    }


    protected AlertsModel(Parcel in) {
        mId = in.readString();
        mTitle = in.readString();
        mMessage = in.readString();
        mAction = in.readString();
        mViewed = in.readByte() != 0;
        mComplaintId = in.readString();
    }

    public static final Creator<AlertsModel> CREATOR = new Creator<AlertsModel>() {
        @Override
        public AlertsModel createFromParcel(Parcel in) {
            return new AlertsModel(in);
        }

        @Override
        public AlertsModel[] newArray(int size) {
            return new AlertsModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mId);
        parcel.writeString(mTitle);
        parcel.writeString(mMessage);
        parcel.writeString(mAction);
        parcel.writeByte((byte) (mViewed ? 1 : 0));
        parcel.writeString(mComplaintId);
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public String getmAction() {
        return mAction;
    }

    public void setmAction(String mAction) {
        this.mAction = mAction;
    }

    public boolean ismViewed() {
        return mViewed;
    }

    public void setmViewed(boolean mViewed) {
        this.mViewed = mViewed;
    }

    public String getmComplaintId() {
        return mComplaintId;
    }

    public void setmComplaintId(String mComplaintId) {
        this.mComplaintId = mComplaintId;
    }
}
