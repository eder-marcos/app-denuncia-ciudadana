package com.ciudadana.denuncia.denunciaciudadana.wrappers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Eder Marcos.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseSetWrapper {

    /**
     * Este es l nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * Para cuando responda la peticion de tipo de transporte
     * */
    @JsonProperty("msg")
    String mMsg;

    public ResponseSetWrapper() {
        /**
         * Funcion vacia
         * */
    }

    public String getmMsg() {
        return mMsg;
    }

    public void setmMsg(String mMsg) {
        this.mMsg = mMsg;
    }
}
