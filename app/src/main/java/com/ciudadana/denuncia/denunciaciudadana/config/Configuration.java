package com.ciudadana.denuncia.denunciaciudadana.config;

/**
 * Created by Sferea.02 on 31/05/2017.
 */

public class Configuration {

    /**
     * Toolbar
     */
    private final String TOOLBAR_COLOR1_KEY = "TOOLBAR_COLOR1_KEY";
    private final String TOOLBAR_COLOR2_KEY = "TOOLBAR_COLOR2_KEY";

    public static int mToolbarColor1;
    public static int mToolbarColor2;

}
