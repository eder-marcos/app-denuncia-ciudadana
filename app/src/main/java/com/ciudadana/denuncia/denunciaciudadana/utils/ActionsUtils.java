package com.ciudadana.denuncia.denunciaciudadana.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Vibrator;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by (Sferea) Eder Marcos on 15/09/2017 at 13.
 * Clase de Utilidad
 * Contiene funciones que se realizan en el dispositivo movil que se ocupan a menudo
 */

public class ActionsUtils {

    public ActionsUtils() {
    }

    /**
     * Funcion de Utilidad
     * Oculta el teclado de la pantalla
     * @param activity: Actividad en el que se ejecuta
     * @param context: Contexto en el que se ejecuta
     * */
    public static void hideKeyboard(Activity activity, Context context) {
        View mView = activity.getCurrentFocus();
        InputMethodManager imm =  (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mView.getWindowToken(), 0);
    }

    /**
     * Funcion de Utilidad
     * Hace vibrar al telefono
     * @param activity: Actividad en el que se ejecuta
     * @param duration: Tiempo que va a durar la vibracion
     * */
    public static void vibrator(Activity activity, int duration) {
        Vibrator mVibrator = (Vibrator) activity.getSystemService(Context.VIBRATOR_SERVICE);
        mVibrator.vibrate(duration);
    }

    /**
     * Funcion de Utilidad
     * Hace vibrar al telefono
     * @param context: Contexto en el que se ejecuta
     * @param duration: Tiempo que va a durar la vibracion
     * */
    public static void vibrator(Context context, int duration) {
        Vibrator mVibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        mVibrator.vibrate(duration);
    }
}
