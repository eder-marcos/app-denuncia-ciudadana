package com.ciudadana.denuncia.denunciaciudadana.utils;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

/**
 * Created by sferea-android on 19/07/2017.
 */

public class GPSLocationListenerUtils extends Service implements android.location.LocationListener {
    private static final String TAG = ConstantsUtils.DEBUG;
    private final Context mContext;
    private final Activity mActivity;

    private boolean isGPSEnabled = false;
    private boolean isNetworkEnabled = false;
    private boolean canGetLocation = false;
    private boolean mPermissionDenied = false;
    private boolean mAccessFineLocation;
    private boolean mAccessCoarseLocation;
    private static final int PERMISSION_REQUEST_CODE = 1;

    private Location location;
    private double mLatitude;
    private double mLongitude;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; /* Indica que son aproximadamente 10 metros */
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;  /* Es 1 minuto (El valor ingresado es en milisegundos) */
    protected LocationManager locationManager;

    public GPSLocationListenerUtils(Context mContext, Activity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        mAccessFineLocation = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        mAccessCoarseLocation = ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        /**
         * Al usar una nueva instancia de esta clase, ejecuta el metodo para verificar si puede obtener la ubicacion
         * */
        getLocation();
    }

    public Location getLocation() {
        try {
            /**
             * Se indica el contexto que se va a utilizar
             * Se indica el servicio que se va a utilizar
             * */
            locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

            /**
             * Obtenemos el estado actual del GPS al igual que el de Interner
             * Si esta apagado o si esta encendido
             * */
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            /**
             * Hace una validacion de los permisos
             * Determina si hay permisos,
             * */
            if (!isGPSEnabled) {
                Log.e(TAG, "GPS deshabilitado");
                this.canGetLocation = false;
            }
            if (!isNetworkEnabled) {
                Log.e(TAG, "WIFI deshabilitado");
                this.canGetLocation = false;
            }

            if (isGPSEnabled) {
                /**
                 * Si el GPS esta encendido, determina la ubicacion usando los servicios de latitud y longitud que proporciona el GPS
                 * */
                if (location == null) {
                    if (mPermissionsExits()) {
                        this.canGetLocation = true;
                        requestLocationUpdates();
                        getLastKnownLocation();
                    }
                }
            } else if (!isGPSEnabled) {
                showSettingsAlert();
            } else if (isNetworkEnabled && mPermissionsExits()) {
                this.canGetLocation = true;
                requestLocationUpdates();
                getLastKnownLocation();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return location;
    }

    private boolean mPermissionsExits() {
        /**
         * Verifica el estado de los permisos
         * @return true cuando tiene permisos
         * Solicita permisos si no los tiene
         * */
        if (!mAccessFineLocation && !mAccessCoarseLocation) {
            /**
             * Verifica si el usuario rechaza la solicitud de permiso en el pasado y selecciona la opción Don't ask again en el diálogo de solicitud de permiso del sistema
             * Muestra una explicacion del porwque necesita los permisos y vuelve a preguntar si los acepta
             * De lo contrario, no hay necesidad de dar explicaciones
             * */
            boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.ACCESS_FINE_LOCATION);
            if (showRationale) {
                showMessageWeNeedPermissions();
            } else {
                ActivityCompat.requestPermissions( mActivity, new String[]{ Manifest.permission.ACCESS_COARSE_LOCATION,  Manifest.permission.ACCESS_FINE_LOCATION }, PERMISSION_REQUEST_CODE);
            }
        } else {
            return true;
        }
        return false;
    }

    private void getLastKnownLocation() {
        /**
         * Obtiene la ultima ubicacion conocida y es almacenada en una variable
         * Se asigan las variable de latitud obtenida del objeto de la ultima ubicacion conocida
         * Se asigan las variable de longitud obtenida del objeto de la ultima ubicacion conocida
         * */
        if (locationManager != null) {
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null) {
                mLatitude = location.getLatitude();
                mLongitude = location.getLongitude();

            }
        }
    }

    private void requestLocationUpdates() {
        /**
         * Peticion para registrarse y poder escuchar las actualizaciones de ubicacion
         * Proveedor
         * Cada cuanto tiempo
         * Tiempo minimo de actualizaciones
         * */
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                MIN_TIME_BW_UPDATES,
                MIN_DISTANCE_CHANGE_FOR_UPDATES,
                this);
    }

    public void removeUpdatesGPS() {
        /**
         * Deja de usar el GPS
         * Es funcion pretende dejar de escuchar cambios, actualizaciones de las ubicaciones en la aplicacion
         * */
        if (locationManager != null) {
            locationManager.removeUpdates(GPSLocationListenerUtils.this);
        }
    }

    public double getLatitude() {
        /**
         * Obtener la Latitud
         * Esta funcion pretende retornar el valor de la laitud obtenida
         * */
        if (location != null) {
            mLatitude = location.getLatitude();
        }

        return mLatitude;
    }

    public double getLongitude() {
        /**
         * Obtener la Longitud
         * Esta funcion pretende retornar el valor de la longitud obtenida
         * */
        if (location != null) {
            mLongitude = location.getLongitude();
        }

        return mLongitude;
    }

    public boolean canGetLocation() {
        /**
         * Verificar si puede obtener la ubicacion
         * @return boolean
         * */
        return this.canGetLocation;
    }

    public boolean isGPSEnabled() {
        /**
         * Verificar si esta habilitado el GPS
         * @return boolean
         * */
        return isGPSEnabled;
    }

    public boolean isNetworkEnabled() {
        /**
         * Verificar si esta habilitado el Wi-Fi
         * @return boolean
         * */
        return isNetworkEnabled;
    }

    public boolean ismPermissionDenied() {
        return mPermissionDenied;
    }

    public void setmPermissionDenied(boolean mPermissionDenied) {
        this.mPermissionDenied = mPermissionDenied;
    }

    public void showSettingsAlert() {
        /**
         * Muestra las configuraciones del dispositivo
         * Esta funcion pretende preguntar si desea ir a las configuraciones del dispositivo para activar el GPS u otorgar permisos necesarios
         * */
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);
        alertDialog.setTitle("GPS deshabilitado");
        alertDialog.setMessage("El GPS no esta habilitado. ¿Deseas ir a la configuracion del dispositivo?");

        /**
         * Boton Aceptar
         * Si el usuario presion el boton de aceptar realiza los siguiente
         * Usa un intent para abrir las configuraciones
         * */
        alertDialog.setPositiveButton("ir a Configuracion", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mActivity.startActivity(intent);
            }
        });

        /**
         * Boton Cancelar
         * Si el usuario presion el boton de cancelar realiza los siguiente
         * Desaparece la alerta
         * */
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    public void showMessageWeNeedPermissions() {
        /**
         * Muestra las configuraciones del dispositivo
         * Esta funcion pretende explicar porque se requiere los permisos
         * */
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);
        alertDialog.setTitle("Se requiere permisos");
        alertDialog.setMessage("Esta aplicacion necesita permisos para poder realizar esta accion");

        /**
         * Boton Aceptar
         * Si el usuario presion el boton de aceptar realiza los siguiente
         * Usa un intent para abrir las configuraciones
         * */
        alertDialog.setPositiveButton("ir a Configuracion", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                final Intent i = new Intent();
                i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                i.addCategory(Intent.CATEGORY_DEFAULT);
                i.setData(Uri.parse("package:" + mContext.getPackageName()));
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                mContext.startActivity(i);
            }
        });

        /**
         * Boton Cancelar
         * Si el usuario presion el boton de cancelar realiza los siguiente
         * Desaparece la alerta
         * */
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }


    /**
     * Implementacion de metodos que por ahoran no se estan ocupando pero que es necesario tenerlos declarados
     * */
    @Override
    public void onLocationChanged(Location location) {
        /**
         * Funcion vacias
         * */
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        /**
         * Funcion vacias
         * */
    }

    @Override
    public void onProviderEnabled(String provider) {
        /**
         * Funcion vacias
         * */
    }

    @Override
    public void onProviderDisabled(String provider) {
        /**
         * Funcion vacias
         * */
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }



}
