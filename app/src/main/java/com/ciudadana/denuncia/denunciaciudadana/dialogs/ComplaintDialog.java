package com.ciudadana.denuncia.denunciaciudadana.dialogs;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.base.ReceiverFragmentDialog;
import com.ciudadana.denuncia.denunciaciudadana.interfaces.ActionDialogListener;

/**
 * Created by Eder Marcos.
 */

public class ComplaintDialog extends ReceiverFragmentDialog {

    /**
     * Variable estatica creada para identificar lo que se imprime en consola y debuguear de mejor manera
     * */

    /**
     * Declaracion de elementos del tipo view u otros
     * */
    private ActionDialogListener mCallback;
    private View mView;
    private ImageView mIconClose;

    /**
     * Declaracion de variables
     * */

    /**
     * Constructor
     * */
    public ComplaintDialog() {
        /**
         * Funcion vacia
         * */
    }

    /**
     * Funcion
     * Funcion sobre escrita
     * */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.dialog_success, container, false);
        /**
         * Localizar elementos
         * */
        mIconClose = mView.findViewById(R.id.ivCloseMessage);

        /**
         * Se inicializan las variables
         * */
        int colorWhite = Color.parseColor("#FFFFFF");
        mIconClose.setImageDrawable(getResources().getDrawable(R.drawable.ic_close));
        mIconClose.setColorFilter(colorWhite);

        return mView;
    }

    /**
     * Funcion
     * Interfaz para la interaccion con actividades o fragmentos
     * */
    public void initCallback(ActionDialogListener mCallback) {
        this.mCallback = mCallback;
    }

    /**
     * Respuesta del servidor
     * */
    @Override
    public void onResponse(Object response) {
        /**
         * Funcion vacia
         * */
    }
}

