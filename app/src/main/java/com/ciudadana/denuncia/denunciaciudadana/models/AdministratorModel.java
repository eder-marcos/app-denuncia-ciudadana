package com.ciudadana.denuncia.denunciaciudadana.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Eder Marcos.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class AdministratorModel implements Parcelable {

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("id")
    private String mId;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("active")
    private boolean mActive;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("createdAt")
    private String mCreatedAt;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("updatedAt")
    private String mUpdatedAt;

    /**
     * Este es el nombre que se recibe en el JSON
     * Debe ser el mismo nombre que esta el response
     * */
    @JsonProperty("user_id")
    private String mUserId;

    /**
     * Funcion
     * Constructor vacio
     * */
    public AdministratorModel() {
        /**
         * Funcion vacia
         * */
    }

    protected AdministratorModel(Parcel in) {
        mId = in.readString();
        mActive = in.readByte() != 0;
        mCreatedAt = in.readString();
        mUpdatedAt = in.readString();
        mUserId = in.readString();
    }

    public static final Creator<AdministratorModel> CREATOR = new Creator<AdministratorModel>() {
        @Override
        public AdministratorModel createFromParcel(Parcel in) {
            return new AdministratorModel(in);
        }

        @Override
        public AdministratorModel[] newArray(int size) {
            return new AdministratorModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mId);
        parcel.writeByte((byte) (mActive ? 1 : 0));
        parcel.writeString(mCreatedAt);
        parcel.writeString(mUpdatedAt);
        parcel.writeString(mUserId);
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public boolean ismActive() {
        return mActive;
    }

    public void setmActive(boolean mActive) {
        this.mActive = mActive;
    }

    public String getmCreatedAt() {
        return mCreatedAt;
    }

    public void setmCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public String getmUpdatedAt() {
        return mUpdatedAt;
    }

    public void setmUpdatedAt(String mUpdatedAt) {
        this.mUpdatedAt = mUpdatedAt;
    }

    public String getmUserId() {
        return mUserId;
    }

    public void setmUserId(String mUserId) {
        this.mUserId = mUserId;
    }
}
