package com.ciudadana.denuncia.denunciaciudadana.utils;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.View;

/**
 * Created by (Sferea) Eder Marcos on 06/09/2017 at 09.
 */

public class SnackbarUtils {

    /**
     * Funcion que imprime en pantalla un mensaje usando un snackbar
     * @param context Contexto en el cual sera mnostrado el snackbar
     * @param view Un snackbar usa un elemento view o un CoordinatorLayout
     * @param msg Es el mensaje que aparecera en el snackbar
     * @param color Es el color que tendra de background
     * */
    public static void showSnackbar(Context context, View view, String msg, int color) {
        Snackbar mSnackbar;
        mSnackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
        View mView = mSnackbar.getView();
        mView.setBackgroundColor(ActivityCompat.getColor(context, color));
        mSnackbar.show();
    }
}
