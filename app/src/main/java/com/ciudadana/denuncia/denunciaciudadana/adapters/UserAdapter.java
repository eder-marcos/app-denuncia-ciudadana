package com.ciudadana.denuncia.denunciaciudadana.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.models.UserModel;

import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Eder Marcos.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.MyViewHolder> {

    /**
     * Variable estatica creada para identificar lo que se imprime en consola y debuguear de mejor manera
     * */

    /**
     * Variables
     * */
    private List<UserModel> mList;

    /**
     * Declaracion de elementos que pertenecen a Android
     * */
    private Context mContext;

    /**
     * Clase
     * Clase MyViewHolder
     * */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mName;
        public TextView mEmail;
        public TextView mCompany;
        public TextView mRole;
        public ImageView mImage;
        public ImageView mBackground;
        public ImageView mIcon1;
        public ImageView mIcon2;
        public ImageView mIcon3;
        public CardView mCardView;

        public MyViewHolder(View view) {
            super(view);
            mCardView = view.findViewById(R.id.cvSupport);
            mImage = view.findViewById(R.id.ivProfileS);
            mBackground = view.findViewById(R.id.ivBackgroundS);
            mName = view.findViewById(R.id.tvNameS);
            mRole = view.findViewById(R.id.tvRoleS);
            mCompany = view.findViewById(R.id.tvCompanyS);
            mEmail = view.findViewById(R.id.tvEmailS);
            mIcon1 = view.findViewById(R.id.ivIcon1);
            mIcon2 = view.findViewById(R.id.ivIcon2);
            mIcon3 = view.findViewById(R.id.ivIcon3);
        }
    }

    /**
     * Constructor
     * */
    public UserAdapter() {
        /**
         * Funcion vacia
         * */
    }

    /**
     * Constructor
     * */
    public UserAdapter(List<UserModel> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public UserAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_support_card, parent, false);

        return new UserAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final UserAdapter.MyViewHolder holder, final int position) {
        UserModel mUser = mList.get(position);
        /**
         * Se establecen los valores correspondientes
         * */
        Glide.with(mContext)
                .load(mUser.getmImageProfile())
                .bitmapTransform(new CropCircleTransformation(mContext))
                .into(holder.mImage);
        holder.mName.setText(mUser.getmName() + " " + mUser.getmLastName());
        holder.mRole.setText(mUser.getmRole());
        holder.mCompany.setText(mUser.getmCompany());
        holder.mEmail.setText(mUser.getmEmail());

        /**
         * Iconos
         * */
        int colorIcon = Color.parseColor("#959599");

        holder.mIcon1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_verified_user));
        holder.mIcon1.setColorFilter(colorIcon);

        holder.mIcon2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_work));
        holder.mIcon2.setColorFilter(colorIcon);

        holder.mIcon3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_email));
        holder.mIcon3.setColorFilter(colorIcon);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
