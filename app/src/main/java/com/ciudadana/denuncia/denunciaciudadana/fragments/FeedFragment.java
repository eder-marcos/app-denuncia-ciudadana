package com.ciudadana.denuncia.denunciaciudadana.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.adapters.FeedAdapter;
import com.ciudadana.denuncia.denunciaciudadana.base.ReceiverFragment;
import com.ciudadana.denuncia.denunciaciudadana.models.ComplaintModel;
import com.ciudadana.denuncia.denunciaciudadana.models.Utils.Response;
import com.ciudadana.denuncia.denunciaciudadana.parser.Parser;
import com.ciudadana.denuncia.denunciaciudadana.utils.ConstantsUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.ConvertTo;
import com.ciudadana.denuncia.denunciaciudadana.utils.GridSpacingItemDecoration;
import com.ciudadana.denuncia.denunciaciudadana.utils.SnackbarUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.StoreSharedPreferencesUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.ValidationsUtils;
import com.ciudadana.denuncia.denunciaciudadana.web.receiver.Receiver;
import com.ciudadana.denuncia.denunciaciudadana.web.request.Request;
import com.ciudadana.denuncia.denunciaciudadana.wrappers.FeedWrapper;
import com.ciudadana.denuncia.denunciaciudadana.wrappers.LoginWrapper;


import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * Created by Eder Marcos.
 */

public class FeedFragment extends ReceiverFragment {

    /**
     * Variable estatica creada para identificar lo que se imprime en consola y debuguear de mejor manera
     * */
    public static final String TAG = ConstantsUtils.DEBUG;
    private FeedAdapter mAdapter;
    private StoreSharedPreferencesUtils mStore;

    /**
     * Variables
     * */
    private List<ComplaintModel> mList;
    private RecyclerView mRecyclerView;
    private View mView;

    /**
     * Funciones sobre escrita
     * */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_feed, container, false);


        /**
         * Localizar elementos
         * */
        mRecyclerView = mFragmentView.findViewById(R.id.rvFeed);
        final CollapsingToolbarLayout collapsingToolbar = mFragmentView.findViewById(R.id.ctFeed);
        AppBarLayout appBarLayout = mFragmentView.findViewById(R.id.abFeed);
        mView = mFragmentView.findViewById(R.id.main_content_feed);

        /**
         * Se inicializan las variables
         * */
        mStore = new StoreSharedPreferencesUtils(getActivity());

        collapsingToolbar.setTitle(" ");
        appBarLayout.setExpanded(true);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbar.setTitle("Feed de denuncias");
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbar.setTitle(" ");
                    isShow = false;
                }
            }
        });
        try {
            Glide.with(mContext)
                    .load(R.drawable.bg_3_3)
                    .bitmapTransform(new BlurTransformation(mContext))
                    .into((ImageView) mFragmentView.findViewById(R.id.ivFeed));
        } catch (Exception e) {
            e.printStackTrace();
        }
        /**
         * Funcion
         * Obtiene la lista de denuncias
         * */
        getComplaints();

        mList = new ArrayList<>();
        mAdapter = new FeedAdapter(mList, mContext);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(mContext, 1);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(
                new GridSpacingItemDecoration(1,
                        ConvertTo.dpToPx(5,
                                mContext.getResources()),
                        true
                ));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        return mFragmentView;
    }

    private void getComplaints() {
        if (!ValidationsUtils.isEmpty(mStore.getData("Token", ""))) {
            try {
                Receiver receiver = new Receiver(
                        FeedFragment.this,
                        Request.Method.GET,
                        Parser.ParserType.RESPONSE,
                        FeedWrapper.class,
                        "Cargando"
                );
                receiver.execute(ConstantsUtils.BASE + "complaint/get/?token=" + mStore.getData("Token", ""), "");
                mRequestQueue.add(receiver);
            } catch (Exception e) {
                Log.e(TAG, "Error al obtener denuncias: ", e);
            }
        } else {
            SnackbarUtils.showSnackbar(getActivity(), mView, "Token requerido", R.color.color_pomegranate);
        }
    }

    /**
     * Funcion sobre escrita
     * Respuesta del servidor
     * */
    @Override
    public void onResponse(Object response) {
        if (response instanceof Response) {
            if (((Response) response).getData() instanceof FeedWrapper) {
                FeedWrapper mWrapper = ((FeedWrapper) ((Response) response).getData());
                mList.addAll(mWrapper.getmList());
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onError(VolleyError error) {
        super.onError(error);
        Log.d(TAG, "onError");
        SnackbarUtils.showSnackbar(getActivity(), mView, error.toString(), R.color.color_pomegranate);
    }
}
