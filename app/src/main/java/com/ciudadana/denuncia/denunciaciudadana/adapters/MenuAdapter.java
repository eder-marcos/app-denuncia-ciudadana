package com.ciudadana.denuncia.denunciaciudadana.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.base.BaseAdapter;
import com.ciudadana.denuncia.denunciaciudadana.models.Utils.NavigationItem;
import com.ciudadana.denuncia.denunciaciudadana.utils.ConstantsUtils;

/**
 * Created by (Sferea) Eder Marcos on 16/08/2017 at 18.
 * Adaptador para renderizar los elementos del menu lateral de esta actividad
 */

public class MenuAdapter extends BaseAdapter<NavigationItem, MenuAdapter.ViewHolder> {

    /**
     * Esta variable siempre es creada en todos los archivos para que cuando se realice DEBUG, se pueda filtrar en la consola con el valor de esta variable
     * */
    private static final String TAG = ConstantsUtils.DEBUG;

    /**
     * Declaracion de elementos del tipo view u otros
     * */
    private OnNavigationItemListener mNavigationListener;

    /**
     * Declaracion de puras variables
     * */

    /**
     * Constructor
     * */
    public MenuAdapter(Context mContext) {
        super(mContext);
    }

    /**
     * Se infla el layout que va a renderizar dentro del menu*/
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.utils_drawer_item_recycler, parent, false);
        /**
         * Nueva instancia del adaptador del Header del menu
         * */
        return new ViewHolder(view);
    }

    /**
     * Cuando un elemento es visible en la pantalla del dispositivo, esta funcion es ejecutada
     * */
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        NavigationItem item = mList.get(position);
        holder.mTitle.setText(item.getmTitle());
    }

    /**
     * Hace referencia a la actividad MyOrders
     * */
    public void setNavigationListener(OnNavigationItemListener navigationListener) {
        mNavigationListener = navigationListener;
    }

    public class ViewHolder extends BaseAdapter.ViewHolder {

        private TextView mTitle;
        private RelativeLayout mContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            mTitle = (TextView) itemView.findViewById(R.id.tvTitleMenuItem);
            mContainer = (RelativeLayout) itemView.findViewById(R.id.rlContainerMenuItem);
        }

        /**
         * Cuando se haga click sobre un elemento del menu, retorna la variable de tipo NavigationItem (pojo)
         * Se pasa como parametro por la interfaz OnItemSelected para comunicar el adaptador con la activity
         * */
        @Override
        public void onClick(View v) {
            Object mOption = mList.get(getLayoutPosition());
            NavigationItem mItem = (NavigationItem) mOption;
            mNavigationListener.OnItemSelected(mItem);
        }
    }

    /**
     * Interfaz que sirve para comunicar el adaptador con la activity
     * */
    public interface OnNavigationItemListener {
        void OnItemSelected(NavigationItem selectedItem);
    }
}
