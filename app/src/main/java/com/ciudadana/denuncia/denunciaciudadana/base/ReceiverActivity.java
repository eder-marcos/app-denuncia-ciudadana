package com.ciudadana.denuncia.denunciaciudadana.base;

/**
 * Created by Sferea.02 on 30/05/2017.
 */

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.android.volley.VolleyError;
import com.ciudadana.denuncia.denunciaciudadana.interfaces.RequestListener;
import com.ciudadana.denuncia.denunciaciudadana.web.receiver.Receiver;

import java.util.ArrayList;

/**
 * Permite facilitar a las actividades las peticiones a los servicios web desplegando el resultado en el
 * método onResponse y los errores en onError.
 * <p>
 * 1. Heredar de esta clase.
 * 2. Obtener la vista padre y asignarsela a mParentView.
 * 3. Implementar la clase abstracta onResponse con el código deseado.
 */
public abstract class ReceiverActivity extends TabLayoutActivity implements RequestListener
{
    protected View mParentView;
    protected final ArrayList<Receiver> mRequestQueue = new ArrayList<>();
    private Dialog mDialog;

    @Override
    public void onStart()
    {
        super.onStart();
        mParentView = findViewById(android.R.id.content);
    }

    /**
     * Cancela la petición cuando el fragmento entra al estado de pausa.
     */
    @Override
    public void onPause()
    {
        super.onPause();
        for(AsyncTask receiver : mRequestQueue)
            receiver.cancel(true);
        mRequestQueue.clear();
    }

    /**
     * Cancela la petición cuando el fragmento entra al estado de paro.
     */
    @Override
    public void onStop()
    {
        super.onStop();
        for(AsyncTask receiver : mRequestQueue)
            receiver.cancel(true);
        mRequestQueue.clear();
    }

    /**
     * Cancela la petición cuando el fragmento es destruido.
     */
    @Override
    public void onDestroy()
    {
        super.onDestroy();
        for(AsyncTask receiver : mRequestQueue)
            receiver.cancel(true);
        mRequestQueue.clear();
    }

    /**
     * Muestra, en un SnackBar, el error que se produjo cuando la petición al servicio web
     * falló.
     *
     * @param error información sobre el error ocurrido.
     */
    @Override
    public void onError(VolleyError error)
    {
        if(error != null)
        {
            if(error.networkResponse == null)
            {
                String buttonsLabels[] = null, title = null, message = null;
                switch(error.getMessage())
                {
//                    case Request.ErrorCode.NO_CONECTION:
//                        buttonsLabels = new String[1];
//                        buttonsLabels[0] = "Ok";
//                        title = "ErrorWrapper de conexión";
//                        message = ConstantsUtil.INTERNET_EXCEPTION_MESSAGE;
//                        break;
//                    case Request.ErrorCode.SERVER:
//                        buttonsLabels = new String[2];
//                        buttonsLabels[0] = "Reintentar";
//                        buttonsLabels[1] = "Cancelar";
//                        title = "Tiempo de espera agotado";
//                        message = "Vuelva a intentarlo mas tarde";
//                        break;
//                    case Request.ErrorCode.TIMEOUT:
//                        buttonsLabels = new String[2];
//                        buttonsLabels[0] = "Reintentar";
//                        buttonsLabels[1] = "Cancelar";
//                        title = "Tiempo de espera agotado";
//                        message = ConstantsUtil.REQUEST_TIMEOUT_MESSAGE;
//                        break;
//                    case Request.ErrorCode.TOKEN_1:
//                    case Request.ErrorCode.TOKEN_2:
//                        invalidTokenDialog();
//                        break;
//                    default:
//                        buttonsLabels = new String[1];
//                        buttonsLabels[0] = "Ok";
//                        title = "Default";
//                        message = "Default";
//                        break;
                }
                if(title != null && message != null && buttonsLabels != null)
                    createErrorDialog(title, message, buttonsLabels);
            }
            else
            {
                switch(error.networkResponse.statusCode)
                {

                }
            }
        }
    }

    /**
     * Permite mostrar un dialogo personalizado en la interfaz de usuario.
     *
     * @param title         título del dialogo.
     * @param message       mensaje del dialogo.
     * @param buttonsString botones que se mostrarán en el dialogo.
     */
    protected void createErrorDialog(final String title, String message, final String[] buttonsString)
    {
        Context context = this;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        final int buttonsNumber = buttonsString.length;
        alertDialogBuilder.setPositiveButton(buttonsString[0], new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss();
                if(buttonsString[0].toLowerCase().contains("reintentar"))
                    makeLastRequest();
//                else if(title.toLowerCase().contains("error de conexión"))
//                    onBackPressed();
            }
        });
        if(buttonsNumber == 2)
        {
            alertDialogBuilder.setNegativeButton(buttonsString[1], new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    dialog.dismiss();
                }
            });
        }
        if(mDialog == null || !mDialog.isShowing())
        {
            mDialog = alertDialogBuilder.create();
            mDialog.setCancelable(false);
            mDialog.show();
        }
    }

    public void makeLastRequest()
    {
        if(mRequestQueue.size() > 0)
        {
            Receiver lastRequest, newRequest;
            lastRequest = mRequestQueue.remove(mRequestQueue.size() - 1);
            if(lastRequest.isUsingDialog())
                newRequest = new Receiver(this, lastRequest.getRequestType(), lastRequest.getParserType(), lastRequest.getClassType(), lastRequest.getDialogMessage());
            else
                newRequest = new Receiver(this, lastRequest.getRequestType(), lastRequest.getParserType(), lastRequest.getClassType(), lastRequest.getSwipeRefreshAnimator());
            newRequest.execute(lastRequest.getRequestData());
            mRequestQueue.add(newRequest);
        }
    }
}