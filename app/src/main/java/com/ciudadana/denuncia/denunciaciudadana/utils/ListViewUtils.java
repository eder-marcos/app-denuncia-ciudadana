package com.ciudadana.denuncia.denunciaciudadana.utils;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Created by (Sferea) Eder Marcos on 18/09/2017 at 12.
 * Clase de Utileria
 */

public class ListViewUtils {

    /**
     * Funcion estatica
     * Calcula el alto dinamico de la lista de acuerdo al alto y numero de los hijos que tiene
     * */
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int mTotalHeight = 0;
        /**
         * Por cada hijo, el alto del list view es modificado
         * */
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            float px = 500 * (listView.getResources().getDisplayMetrics().density);
            listItem.measure(View.MeasureSpec.makeMeasureSpec((int)px, View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            mTotalHeight += listItem.getMeasuredHeight();
        }
        // Get total height of all item dividers.
        int totalDividersHeight = listView.getDividerHeight() *
                (listAdapter.getCount() - 1);
        // Get padding
        int totalPadding = listView.getPaddingTop() + listView.getPaddingBottom();
        /**
         * Se le asiga el alto obtenido
         * */
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = mTotalHeight + totalDividersHeight + totalPadding + 20;
        listView.setLayoutParams(params);
    }
}
