package com.ciudadana.denuncia.denunciaciudadana.interfaces;

/**
 * Created by Eder Marcos.
 * Interfaz que comunica un dialogo con un fragmento o actividad
 * Su funcion es saber que va a realizar el usuario cuando el usuario presion Cancelar o Aceptar dentro de un dialogo
 */

public interface ActionDialogListener {
    /**
     * Si el usuario presiono el boton aceptar (true)
     * Se el usuario no presiono el boton aceptar (false)
     * Si es nulo, el usuario no presiono ningun boton
     * */
    public void onAction(boolean reponse);
}