package com.ciudadana.denuncia.denunciaciudadana.utils;

import android.app.Activity;
import android.util.Patterns;
import android.view.View;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by (Sferea) Eder Marcos on 22/08/2017 at 15.
 * Clase de Utileria
 */

public class ValidationsUtils {

    /**
     * Declaracion de puras variables
     * */
    public static Pattern mEmail = Patterns.EMAIL_ADDRESS;
    public static Pattern mLetters= Pattern.compile("[A-Za-z,. 0-9_ÑñáéíóúÁÉÍÓÚ]+");
    public static Pattern mRFC= Pattern.compile("[A-Za-z0-9]");
    public static Pattern mNumbers = Pattern.compile("[0-9]");
    public static Pattern mWithoutZero = Pattern.compile("[1-9]");
    public static Matcher mMatcher;

    /**
     * Valida que la cadena no este vacia, pero que sea un caracter valido
     * Usa una expresion regular para hacer esta validacion
     * @param data Cadena de texto o de numeros que recibe
     * */
    public static boolean isEmpty(String data) {
        return data == null || data == "" || data.length() <= 0;
    }

    /**
     * Valida que la cadena no este vacia, pero que sea un caracter valido
     * Usa una expresion regular para hacer esta validacion
     * @param data Cadena de texto o de numeros que recibe
     * @param size Se especifica la longitud minima
     * */
    public static boolean isEmpty(String data, int size) {
        return data == null || data == "" || data.length() <= size;
    }

    /**
     * Validacion de los numeros, que sea un dato valido, que no este vacio etc.
     * Usa una expresion regular para que admita solo numeros
     * @param data Cadena de texto o de numeros que recibe
     * @param withoutZero Si es true valida que sea numero pero != 0. (1-9)
     * */
    public static boolean isEmpty(String data, boolean withoutZero) {
        if (withoutZero) {
            mMatcher = mWithoutZero.matcher(data);
        } else {
            mMatcher = mNumbers.matcher(data);
        }
            return (!mMatcher.find());
    }

    /**
     * Validacion de solo letras
     * Retorna true si esta bien
     * Retorna false si el parametro no es una letra
     * */
    public static boolean validationLetters(String data) {
        mMatcher = mLetters.matcher(data);
        return (mMatcher.find());
    }

    /**
     * Validacion de email
     * Retorna true si esta bien
     * Retorna false si el parametro no es un correo electronico valido
     * */
    public static boolean validationEmail(String data) {
        mMatcher = mEmail.matcher(data);
        return (mMatcher.find());
    }

    /**
     * Validacion de solo numeros
     * Retorna true si esta bien
     * Retorna false si el parametro no es un numero
     * */
    public static boolean validationNumbers(String data) {
        mMatcher = mNumbers.matcher(data);
        return (mMatcher.find());
    }

    /**
     * Validacion de un RFC
     * Retorna true si esta bien
     * Retorna false si el parametro no es un numero
     * */
    public static boolean validationRFC(String data) {
        mMatcher = mRFC.matcher(data);
        return (mMatcher.find());
    }

    /**
     * Ingresa los datos en un TextView
     * */
    public static void setData(Activity activity, int id, String msg) {
        if (isEmpty(msg))
            msg = "----";
        ((TextView) activity.findViewById(id)).setText(msg);
    }

    /**
     * Ingresa los datos en un TextView
     * */
    public static void setData(View view, int id, String msg) {
        if (isEmpty(msg))
            msg = "----";
        ((TextView) view.findViewById(id)).setText(msg);
    }
}
