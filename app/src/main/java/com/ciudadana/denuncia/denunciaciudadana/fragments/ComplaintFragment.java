package com.ciudadana.denuncia.denunciaciudadana.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.adapters.ComplaintAdapter;
import com.ciudadana.denuncia.denunciaciudadana.adapters.CustomPageAdapter;
import com.ciudadana.denuncia.denunciaciudadana.base.ReceiverFragment;
import com.ciudadana.denuncia.denunciaciudadana.interfaces.ActionDialogListener;
import com.ciudadana.denuncia.denunciaciudadana.models.ComplaintModel;
import com.ciudadana.denuncia.denunciaciudadana.models.EvidenceModel;
import com.ciudadana.denuncia.denunciaciudadana.models.UserModel;
import com.ciudadana.denuncia.denunciaciudadana.models.Utils.Response;
import com.ciudadana.denuncia.denunciaciudadana.parser.Parser;
import com.ciudadana.denuncia.denunciaciudadana.utils.ConstantsUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.ImageUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.PrefManager;
import com.ciudadana.denuncia.denunciaciudadana.utils.RealPathUtil;
import com.ciudadana.denuncia.denunciaciudadana.utils.SnackbarUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.StoreSharedPreferencesUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.ValidationsUtils;
import com.ciudadana.denuncia.denunciaciudadana.web.receiver.Receiver;
import com.ciudadana.denuncia.denunciaciudadana.web.request.Request;
import com.ciudadana.denuncia.denunciaciudadana.wrappers.FeedWrapper;
import com.ciudadana.denuncia.denunciaciudadana.wrappers.ResponseSetWrapper;
import com.ciudadana.denuncia.denunciaciudadana.wrappers.UserWrapper;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static com.google.android.gms.internal.zzagy.runOnUiThread;

/**
 * Created by Eder Marcos.
 */

public class ComplaintFragment extends ReceiverFragment {

    /**
     * Variable estatica creada para identificar lo que se imprime en consola y debuguear de mejor manera
     * */

    /**
     * Variables
     * */
    private String TAG = ConstantsUtils.DEBUG;
    private TextView[] mDots;
    private int[] mLayouts;
    private PrefManager mPrefManager;
    private StoreSharedPreferencesUtils mStore;
    private View mView;
    private static final int REQUEST_CAMERA = 1001;
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int REQUEST_GALLERY = 1002;
    private final CharSequence[] items = {"Tomar una foto", "Seleccionar de la galeria"};

    /**
     * Declaracion de elementos que pertenecen a Android
     * */
    private ViewPager mViewPager;
    private CustomPageAdapter mAdapter;
    private LinearLayout mLayout;
    private Button btnBack, btnNext;
    private Uri selectedImageUri;
    private Bitmap mBitmap;

    /**
     * Funciones sobre escrita
     * */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_complaint, container, false);
        /**
         * Localizar elementos
         * */
        mViewPager = mFragmentView.findViewById(R.id.view_pager);
        mLayout = mFragmentView.findViewById(R.id.layoutDots);
        btnBack = mFragmentView.findViewById(R.id.btn_back);
        btnNext = mFragmentView.findViewById(R.id.btn_next);
        mView = mFragmentView.findViewById(R.id.main_content_complaint);

        /**
         * Se inicializan las variables
         * */
        mStore = new StoreSharedPreferencesUtils(getActivity());
        if (ValidationsUtils.isEmpty(mStore.getData("profile", ""))) {
            getProfile();
        }
        mLayouts = new int[] {
                R.layout.page_complaint_type,
                R.layout.page_complaint_form,
                R.layout.page_complaint_evidence
        };
        mAdapter = new CustomPageAdapter(mLayouts, getContext(), ComplaintFragment.this);
        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        /**
         * Se agregaran los indicadores
         * */
        addBottomDots(0);
        /**
         * Acciones de los botones de la parte inferior
         * */
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Se salta todas las paginas
                 * */
                int current = getItem(0);
                if (current > 0) {
                    mViewPager.setCurrentItem(current - 1);
                }
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /**
                 * Verifica si se encuentra en la ultima pagina, si es asi, lanza la siguiente actividad
                 * */
                int current = getItem(+1);
                if (current < mLayouts.length) {
                    // move to next screen
                    mViewPager.setCurrentItem(current);
                } else {
                    launchHomeScreen();
                }
            }
        });

        return mFragmentView;
    }

    public void openFileChooserDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Agregar evidencia");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        initCameraPermission();
                        break;
                    case 1:
                        initGalleryIntent();
                        break;
                    default:
                }
            }
        });
        builder.show();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void initCameraPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                Toast.makeText(getActivity(), "Permiso para usar la camara", Toast.LENGTH_SHORT).show();
            }
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION);
        } else {
            initCameraIntent();
        }
    }

    private void initGalleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, REQUEST_GALLERY);
    }

    private void initCameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = getOutputMediafile(1);
        selectedImageUri = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImageUri);
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    private File getOutputMediafile(int type) {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), getResources().getString(R.string.app_name)
        );
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyHHdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".png");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initCameraIntent();
            } else {
                Toast.makeText(getActivity(), "Los permisos fueron negados por el usuario", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    String realPath = "";
    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                realPath = selectedImageUri.getPath();
            } else if (requestCode == REQUEST_GALLERY) {
                selectedImageUri = data.getData();
                if (Build.VERSION.SDK_INT < 11) {
                    realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(getActivity(), data.getData());
                } else if (Build.VERSION.SDK_INT < 19) {
                    realPath = RealPathUtil.getRealPathFromURI_API11to18(getActivity(), data.getData());
                } else {
                    realPath = RealPathUtil.getRealPathFromURI_API19(getActivity(), data.getData());
                }

                Log.d(TAG, "real path: " + realPath);
            }
            mBitmap = ImageUtils.getScaledImage(selectedImageUri, getActivity());
            mAdapter.setImage(mBitmap, R.id.ivEvidence1);
        }
    }

    private void execMultipartPost() throws Exception {
//        Uri uriFromPath = Uri.fromFile(new File(realPath));
        File file = new File(realPath);
        String contentType = file.toURL().openConnection().getContentType();

        Log.d(TAG, "file: " + file.getPath());
        Log.d(TAG, "contentType: " + contentType);


        RequestBody fileBody = RequestBody.create(MediaType.parse(contentType), file);

        final String filename = "file_" + System.currentTimeMillis() / 1000L;

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("user_id", "1")
                .addFormDataPart("group_id", "1")
                .addFormDataPart("token", "NVbk9J_eE@ux2v?3")
                .addFormDataPart("image", filename + ".jpg", fileBody)
                .build();

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url("http://192.168.1.184/Dropbox/Upload/PHP/AndroidFileUpload/fileUpload.php")
                .post(requestBody)
                .build();

        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "run: " + e.getMessage());
                        Toast.makeText(mContext, "Imagen subida", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final okhttp3.Response response) throws IOException {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {

                            Gson gson = new Gson();
                            EvidenceModel evidenceModel = gson.fromJson(mStore.getData("evidenceModel", ""), EvidenceModel.class);
                            evidenceModel.setmImageSource(response.body().string().trim());

                            /**
                             * Se guardan los datos
                             * */
                            mStore.saveData("evidenceModel", gson.toJson(evidenceModel));

                            setEvidence();

                            Log.e(TAG, "NameFile: " + response.body().string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }





    /**
     * Funcion
     * Retorna la posicion actual del item
     * */
    private int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }

    /**
     * Funcion
     * Indica si es la primera vez que se inicializa la aplicacion
     * Inicializa la siguiente actividad
     * */
    private void launchHomeScreen() {
        setComplaint();
    }

    /**
     * Funcion
     * Agrega el indicador de la pagina actual
     * */
    private void addBottomDots(int currentPage) {
        mDots = new TextView[mLayouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        mLayout.removeAllViews();
        for (int i = 0; i < mDots.length; i++) {
            mDots[i] = new TextView(getActivity());
            mDots[i].setText(Html.fromHtml("&#8226;"));
            mDots[i].setTextSize(35);
            mDots[i].setTextColor(colorsInactive[currentPage]);
            mLayout.addView(mDots[i]);
        }

        if (mDots.length > 0)
            mDots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    /**
     * Variable
     * ViewPager Listener
     * Detecta cuando cambia de pagina
     * */
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            /**
             * Se agrega el indicador de la pagina en la posicion de la pagina actual
             * */
            addBottomDots(position);
            /**
             * Cambia las paginas con los botones de la parte inferior
             * */
            if (position == mLayouts.length - 1) {
                /**
                 * La ultima pagina cambia el texto a COMENZAR
                 * */
                btnNext.setText(getString(R.string.send));
            } else if(position == 0) {
                btnBack.setVisibility(View.GONE);
                btnBack.setText(getString(R.string.back));
                btnNext.setText(getString(R.string.next));
            } else {
                /**
                 * Cambia el texto a Siguiente de la pagina 0 hasta la ante penultima
                 * */
                btnBack.setVisibility(View.VISIBLE);
                btnBack.setText(getString(R.string.back));
                btnNext.setText(getString(R.string.next));
            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            /**
             * Funcion vacia
             * */
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
            /**
             * Funcion vacia
             * */
        }
    };

    /**
     * Funcion de Utileria
     * Obtiene el perfil del usuario para despues poder usar sus datos
     * */
    private void getProfile() {
        String email = mStore.getData("email", "");
        String token = mStore.getData("Token", "");
        if (!ValidationsUtils.isEmpty(token)
                && !ValidationsUtils.isEmpty(email)) {
            try {
                HashMap<String, Object> mParams = new HashMap<>();
                mParams.put("email", email);
                Receiver receiver = new Receiver(
                        ComplaintFragment.this,
                        Request.Method.JSON,
                        Parser.ParserType.RESPONSE,
                        UserWrapper.class,
                        "Cargando"
                );
                receiver.execute(
                        ConstantsUtils.BASE + "user/searchName/?token=" + token,
                        "",
                        new JSONObject(mParams).toString());
                mRequestQueue.add(receiver);
            } catch (Exception e) {
                Log.e(TAG, "Error al obtener perfil: ", e);
            }
        } else {
            SnackbarUtils.showSnackbar(getActivity(), mView, "Datos incompletos", R.color.color_pomegranate);
        }
    }

    private void setEvidence() {
        String token = mStore.getData("Token", "");

        Gson gson = new Gson();
        String json = mStore.getData("evidenceModel", "");
        EvidenceModel evidenceModel = gson.fromJson(json, EvidenceModel.class);

        if (!ValidationsUtils.isEmpty(token)) {
            try {
                HashMap<String, Object> mParams = new HashMap<>();
                mParams.put("complaint_id", evidenceModel.getmComplaintId());
                mParams.put("imageSource", evidenceModel.getmImageSource());
                Receiver receiver = new Receiver(
                        ComplaintFragment.this,
                        Request.Method.JSON,
                        Parser.ParserType.RESPONSE,
                        ResponseSetWrapper.class,
                        "Cargando"
                );
                receiver.execute(
                        ConstantsUtils.BASE + "evidence/set/?token=" + token,
                        "",
                        new JSONObject(mParams).toString());
                mRequestQueue.add(receiver);
            } catch (Exception e) {
                Log.e(TAG, "Error al agregar evidencia: ", e);
            }
        }
    }

    private void setComplaint() {
        String token = mStore.getData("Token", "");
        /**
         * Se obtiene el modelo Complaint
         * */
        Gson gson = new Gson();
        String json = mStore.getData("complaintModel", "");
        ComplaintModel complaintModel = gson.fromJson(json, ComplaintModel.class);
        UserModel userModel = gson.fromJson(mStore.getData("profile", ""), UserModel.class);

        /**
         * Valicaciones antes de hacer la peticion
         * */
        if (!ValidationsUtils.isEmpty(token)) {
            try {
                HashMap<String, Object> mParams = new HashMap<>();
                mParams.put("typeComplaint", complaintModel.getmTipeComplaint());
                mParams.put("title", complaintModel.getmTitle());
                mParams.put("description", complaintModel.getmDescription());
                mParams.put("latitude", complaintModel.getmLatitude());
                mParams.put("longitude", complaintModel.getmLongitude());
                mParams.put("user_id", userModel.getmId());
                Receiver receiver = new Receiver(
                        ComplaintFragment.this,
                        Request.Method.JSON,
                        Parser.ParserType.RESPONSE,
                        ResponseSetWrapper.class,
                        "Cargando"
                );
                receiver.execute(
                        ConstantsUtils.BASE + "complaint/set/?token=" + token,
                        "",
                        new JSONObject(mParams).toString());
                mRequestQueue.add(receiver);
            } catch (Exception e) {
                Log.e(TAG, "Error agregar una denuncia: ", e);
            }
        }
    }

    /**
     * Respuesta del servidor
     * */
    @Override
    public void onResponse(Object response) {
        Gson gson = new Gson();

        if (response instanceof Response) {
            if (((Response) response).getData() instanceof UserWrapper) {
                UserModel user =  ((UserWrapper) ((Response) response).getData()).getmUser();
                /**
                 * Se guarda los datos del perfil en shared preferences
                 * */
                String json = gson.toJson(user);
                mStore.saveData("profile", json);
            } else if (((Response) response).getData() instanceof ResponseSetWrapper) {
                String id = ((ResponseSetWrapper) ((Response) response).getData()).getmMsg();
                if (id != null) {

                    /**
                     * Se obtienen los datos
                     * */
                    EvidenceModel evidenceModel = new EvidenceModel();
                    evidenceModel.setmComplaintId(id);
                    mStore.saveData("evidenceModel", gson.toJson(evidenceModel));

                    /**
                     * Se agrega la evidencia
                     * */
                    try {
                        execMultipartPost();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
