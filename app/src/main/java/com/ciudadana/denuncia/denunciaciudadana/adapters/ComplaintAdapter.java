package com.ciudadana.denuncia.denunciaciudadana.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ciudadana.denuncia.denunciaciudadana.R;
import com.ciudadana.denuncia.denunciaciudadana.models.ComplaintModel;
import com.ciudadana.denuncia.denunciaciudadana.models.UserModel;
import com.ciudadana.denuncia.denunciaciudadana.utils.StoreSharedPreferencesUtils;
import com.ciudadana.denuncia.denunciaciudadana.utils.ValidationsUtils;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by Eder Marcos.
 */

public class ComplaintAdapter extends RecyclerView.Adapter<ComplaintAdapter.MyViewHolder> {

    /**
     * Variable estatica creada para identificar lo que se imprime en consola y debuguear de mejor manera
     * */

    /**
     * Variables
     * */
    private List<ComplaintModel> mList;
    private StoreSharedPreferencesUtils mStore;

    /**
     * Declaracion de elementos que pertenecen a Android
     * */
    private Context mContext;

    /**
     * Clase
     * Clase MyViewHolder
     * */
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTitle;
        public ImageView mIcon;
        public CardView mContainer;

        public MyViewHolder(View view) {
            super(view);
            mIcon = view.findViewById(R.id.ivIconC);
            mTitle = view.findViewById(R.id.tvTitleC);
            mContainer = view.findViewById(R.id.cvTypeComplaint);
        }
    }

    /**
     * Constructor
     * */
    public ComplaintAdapter() {
        /**
         * Funcion vacia
         * */
    }

    /**
     * Constructor
     * */
    public ComplaintAdapter(List<ComplaintModel> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.page_complaint_type_card, parent, false);
        mStore = new StoreSharedPreferencesUtils(mContext);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        ComplaintModel mTypeComplaint = mList.get(position);
        /**
         * Se establecen los valores correspondientes
         * */
        holder.mTitle.setText(mTypeComplaint.getmTitle());
        Glide.with(mContext).load(mTypeComplaint.getmBgDrawable()).into(holder.mIcon);
        holder.mContainer.setCardBackgroundColor(mContext.getResources().getColor(mTypeComplaint.getmBgColor()));
        /**
         * Se detecta el evento click sobre el cardview
         * */
        holder.mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (position) {
                    case 0:
                        setData(
                                "Delitos informaticos",
                                mList.get(0).getmBgDrawable(),
                                mList.get(0).getmBgColor());
                        break;

                    case 1:
                        setData(
                                "Robos",
                                mList.get(1).getmBgDrawable(),
                                mList.get(1).getmBgColor());
                        break;

                    case 2:
                        setData(
                                "Extorsion",
                                mList.get(2).getmBgDrawable(),
                                mList.get(2).getmBgColor());
                        break;

                    case 3:
                        setData(
                                "Estafas",
                                mList.get(3).getmBgDrawable(),
                                mList.get(3).getmBgColor());
                        break;

                    case 4:
                        setData(
                                "Secuestros",
                                mList.get(4).getmBgDrawable(),
                                mList.get(4).getmBgColor());
                        break;

                    case 5:
                        setData(
                                "Delito Electoral",
                                mList.get(5).getmBgDrawable(),
                                mList.get(5).getmBgColor());
                        break;
                }

                /**
                 * Cada que se elige un tipo, se pintan los datos
                 * */
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    private void setData(String type, int drawable, int color) {

        Gson gson = new Gson();
        String json = mStore.getData("profile", "");
        UserModel user = gson.fromJson(json, UserModel.class);

        /**
         * Almacenar datos
         * */
        mStore.saveData("typeComplaint", type);
        mStore.saveData("drawableComplaint", drawable);
        mStore.saveData("colorComplaint", color);
        mStore.saveData("user_id", user.getmId());

        /**
         * Set tipo de denuncia
         * */
        ((TextView) ((Activity)mContext).findViewById(R.id.tvTypeComplaint)).setText(type);

        if (!ValidationsUtils.isEmpty(json)) {
            /**
             * Set Victima
             * */
            ((TextView) ((Activity)mContext).findViewById(R.id.tvVictim)).setText(user.getmName() + " " + user.getmLastName());
//            ((TextView) ((Activity)mContext).findViewById(R.id.tvVictim3)).setText(user.getmName() + " " + user.getmLastName());
        }
    }
}
